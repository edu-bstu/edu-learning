package ru.edu.bstu.learning.web.rest;

import ru.edu.bstu.learning.service.MarkService;
import ru.edu.bstu.learning.web.rest.errors.BadRequestAlertException;
import ru.edu.bstu.learning.service.dto.MarkDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ru.edu.bstu.learning.domain.Mark}.
 */
@RestController
@RequestMapping("/api")
public class MarkResource {

    private final Logger log = LoggerFactory.getLogger(MarkResource.class);

    private static final String ENTITY_NAME = "mark";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MarkService markService;

    public MarkResource(MarkService markService) {
        this.markService = markService;
    }

    /**
     * {@code POST  /marks} : Create a new mark.
     *
     * @param markDTO the markDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new markDTO, or with status {@code 400 (Bad Request)} if the mark has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/marks")
    public ResponseEntity<MarkDTO> createMark(@Valid @RequestBody MarkDTO markDTO) throws URISyntaxException {
        log.debug("REST request to save Mark : {}", markDTO);
        if (markDTO.getId() != null) {
            throw new BadRequestAlertException("A new mark cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MarkDTO result = markService.save(markDTO);
        return ResponseEntity.created(new URI("/api/marks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /marks} : Updates an existing mark.
     *
     * @param markDTO the markDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated markDTO,
     * or with status {@code 400 (Bad Request)} if the markDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the markDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/marks")
    public ResponseEntity<MarkDTO> updateMark(@Valid @RequestBody MarkDTO markDTO) throws URISyntaxException {
        log.debug("REST request to update Mark : {}", markDTO);
        if (markDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MarkDTO result = markService.save(markDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, markDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /marks} : get all the marks.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of marks in body.
     */
    @GetMapping("/marks")
    public ResponseEntity<List<MarkDTO>> getAllMarks(Pageable pageable) {
        log.debug("REST request to get a page of Marks");
        Page<MarkDTO> page = markService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /marks/:id} : get the "id" mark.
     *
     * @param id the id of the markDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the markDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/marks/{id}")
    public ResponseEntity<MarkDTO> getMark(@PathVariable Long id) {
        log.debug("REST request to get Mark : {}", id);
        Optional<MarkDTO> markDTO = markService.findOne(id);
        return ResponseUtil.wrapOrNotFound(markDTO);
    }

    /**
     * {@code DELETE  /marks/:id} : delete the "id" mark.
     *
     * @param id the id of the markDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/marks/{id}")
    public ResponseEntity<Void> deleteMark(@PathVariable Long id) {
        log.debug("REST request to delete Mark : {}", id);
        markService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
