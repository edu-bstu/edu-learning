/**
 * View Models used by Spring MVC REST controllers.
 */
package ru.edu.bstu.learning.web.rest.vm;
