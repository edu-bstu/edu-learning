package ru.edu.bstu.learning.web.rest;

import ru.edu.bstu.learning.service.ScheduleLessonService;
import ru.edu.bstu.learning.web.rest.errors.BadRequestAlertException;
import ru.edu.bstu.learning.service.dto.ScheduleLessonDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ru.edu.bstu.learning.domain.ScheduleLesson}.
 */
@RestController
@RequestMapping("/api")
public class ScheduleLessonResource {

    private final Logger log = LoggerFactory.getLogger(ScheduleLessonResource.class);

    private static final String ENTITY_NAME = "scheduleLesson";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ScheduleLessonService scheduleLessonService;

    public ScheduleLessonResource(ScheduleLessonService scheduleLessonService) {
        this.scheduleLessonService = scheduleLessonService;
    }

    /**
     * {@code POST  /schedule-lessons} : Create a new scheduleLesson.
     *
     * @param scheduleLessonDTO the scheduleLessonDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new scheduleLessonDTO, or with status {@code 400 (Bad Request)} if the scheduleLesson has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/schedule-lessons")
    public ResponseEntity<ScheduleLessonDTO> createScheduleLesson(@Valid @RequestBody ScheduleLessonDTO scheduleLessonDTO) throws URISyntaxException {
        log.debug("REST request to save ScheduleLesson : {}", scheduleLessonDTO);
        if (scheduleLessonDTO.getId() != null) {
            throw new BadRequestAlertException("A new scheduleLesson cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ScheduleLessonDTO result = scheduleLessonService.save(scheduleLessonDTO);
        return ResponseEntity.created(new URI("/api/schedule-lessons/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /schedule-lessons} : Updates an existing scheduleLesson.
     *
     * @param scheduleLessonDTO the scheduleLessonDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated scheduleLessonDTO,
     * or with status {@code 400 (Bad Request)} if the scheduleLessonDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the scheduleLessonDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/schedule-lessons")
    public ResponseEntity<ScheduleLessonDTO> updateScheduleLesson(@Valid @RequestBody ScheduleLessonDTO scheduleLessonDTO) throws URISyntaxException {
        log.debug("REST request to update ScheduleLesson : {}", scheduleLessonDTO);
        if (scheduleLessonDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ScheduleLessonDTO result = scheduleLessonService.save(scheduleLessonDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, scheduleLessonDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /schedule-lessons} : get all the scheduleLessons.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of scheduleLessons in body.
     */
    @GetMapping("/schedule-lessons")
    public ResponseEntity<List<ScheduleLessonDTO>> getAllScheduleLessons(Pageable pageable) {
        log.debug("REST request to get a page of ScheduleLessons");
        Page<ScheduleLessonDTO> page = scheduleLessonService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /schedule-lessons/:id} : get the "id" scheduleLesson.
     *
     * @param id the id of the scheduleLessonDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the scheduleLessonDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/schedule-lessons/{id}")
    public ResponseEntity<ScheduleLessonDTO> getScheduleLesson(@PathVariable Long id) {
        log.debug("REST request to get ScheduleLesson : {}", id);
        Optional<ScheduleLessonDTO> scheduleLessonDTO = scheduleLessonService.findOne(id);
        return ResponseUtil.wrapOrNotFound(scheduleLessonDTO);
    }

    /**
     * {@code DELETE  /schedule-lessons/:id} : delete the "id" scheduleLesson.
     *
     * @param id the id of the scheduleLessonDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/schedule-lessons/{id}")
    public ResponseEntity<Void> deleteScheduleLesson(@PathVariable Long id) {
        log.debug("REST request to delete ScheduleLesson : {}", id);
        scheduleLessonService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
