package ru.edu.bstu.learning.repository;

import ru.edu.bstu.learning.domain.ScheduleLesson;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ScheduleLesson entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ScheduleLessonRepository extends JpaRepository<ScheduleLesson, Long> {
}
