package ru.edu.bstu.learning.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Специальность
 */
@Entity
@Table(name = "specialty")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Specialty implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Название
     */
    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    /**
     * Код
     */
    @NotNull
    @Column(name = "code", nullable = false)
    private String code;

    @OneToMany(mappedBy = "specialty")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Group> groups = new HashSet<>();

    /**
     * Институт специальности
     */
    @ManyToOne
    @JsonIgnoreProperties(value = "specialties", allowSetters = true)
    private Institution institution;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Specialty name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public Specialty code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Set<Group> getGroups() {
        return groups;
    }

    public Specialty groups(Set<Group> groups) {
        this.groups = groups;
        return this;
    }

    public Specialty addGroup(Group group) {
        this.groups.add(group);
        group.setSpecialty(this);
        return this;
    }

    public Specialty removeGroup(Group group) {
        this.groups.remove(group);
        group.setSpecialty(null);
        return this;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    public Institution getInstitution() {
        return institution;
    }

    public Specialty institution(Institution institution) {
        this.institution = institution;
        return this;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Specialty)) {
            return false;
        }
        return id != null && id.equals(((Specialty) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Specialty{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            "}";
    }
}
