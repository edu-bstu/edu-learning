package ru.edu.bstu.learning.domain.enumeration;

/**
 * The MarkType enumeration.
 */
public enum MarkType {
    SCORE, ABSENCE, OFFSET, NEGLECT
}
