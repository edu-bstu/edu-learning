package ru.edu.bstu.learning.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Студент
 */
@Entity
@Table(name = "student")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Номер зачетной книжки
     */
    @NotNull
    @Column(name = "record_book_number", nullable = false)
    private String recordBookNumber;

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private Person person;

    @OneToMany(mappedBy = "student")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Mark> marks = new HashSet<>();

    /**
     * Группа студента
     */
    @ManyToOne
    @JsonIgnoreProperties(value = "students", allowSetters = true)
    private Group group;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecordBookNumber() {
        return recordBookNumber;
    }

    public Student recordBookNumber(String recordBookNumber) {
        this.recordBookNumber = recordBookNumber;
        return this;
    }

    public void setRecordBookNumber(String recordBookNumber) {
        this.recordBookNumber = recordBookNumber;
    }

    public Person getPerson() {
        return person;
    }

    public Student person(Person person) {
        this.person = person;
        return this;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Set<Mark> getMarks() {
        return marks;
    }

    public Student marks(Set<Mark> marks) {
        this.marks = marks;
        return this;
    }

    public Student addMark(Mark mark) {
        this.marks.add(mark);
        mark.setStudent(this);
        return this;
    }

    public Student removeMark(Mark mark) {
        this.marks.remove(mark);
        mark.setStudent(null);
        return this;
    }

    public void setMarks(Set<Mark> marks) {
        this.marks = marks;
    }

    public Group getGroup() {
        return group;
    }

    public Student group(Group group) {
        this.group = group;
        return this;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Student)) {
            return false;
        }
        return id != null && id.equals(((Student) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Student{" +
            "id=" + getId() +
            ", recordBookNumber='" + getRecordBookNumber() + "'" +
            "}";
    }
}
