package ru.edu.bstu.learning.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * Урок
 */
@Entity
@Table(name = "lesson")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Lesson implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Дата урока
     */
    @NotNull
    @Column(name = "date", nullable = false)
    private LocalDate date;

    @OneToMany(mappedBy = "lesson")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Mark> marks = new HashSet<>();

    /**
     * Предмет урока
     */
    @ManyToOne
    @JsonIgnoreProperties(value = "lessons", allowSetters = true)
    private Subject subject;

    /**
     * Преподаватель урока
     */
    @ManyToOne
    @JsonIgnoreProperties(value = "lessons", allowSetters = true)
    private Employee teacher;

    /**
     * Расписание урока
     */
    @ManyToOne
    @JsonIgnoreProperties(value = "lessons", allowSetters = true)
    private ScheduleLesson schedule;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public Lesson date(LocalDate date) {
        this.date = date;
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Set<Mark> getMarks() {
        return marks;
    }

    public Lesson marks(Set<Mark> marks) {
        this.marks = marks;
        return this;
    }

    public Lesson addMark(Mark mark) {
        this.marks.add(mark);
        mark.setLesson(this);
        return this;
    }

    public Lesson removeMark(Mark mark) {
        this.marks.remove(mark);
        mark.setLesson(null);
        return this;
    }

    public void setMarks(Set<Mark> marks) {
        this.marks = marks;
    }

    public Subject getSubject() {
        return subject;
    }

    public Lesson subject(Subject subject) {
        this.subject = subject;
        return this;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Employee getTeacher() {
        return teacher;
    }

    public Lesson teacher(Employee employee) {
        this.teacher = employee;
        return this;
    }

    public void setTeacher(Employee employee) {
        this.teacher = employee;
    }

    public ScheduleLesson getSchedule() {
        return schedule;
    }

    public Lesson schedule(ScheduleLesson scheduleLesson) {
        this.schedule = scheduleLesson;
        return this;
    }

    public void setSchedule(ScheduleLesson scheduleLesson) {
        this.schedule = scheduleLesson;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Lesson)) {
            return false;
        }
        return id != null && id.equals(((Lesson) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Lesson{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            "}";
    }
}
