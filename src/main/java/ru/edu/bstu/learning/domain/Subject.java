package ru.edu.bstu.learning.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Изучаемый предмет
 */
@Entity
@Table(name = "subject")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Subject implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Название
     */
    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    /**
     * Код
     */
    @Column(name = "code")
    private String code;

    @OneToMany(mappedBy = "subject")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Lesson> lessons = new HashSet<>();

    @OneToMany(mappedBy = "subject")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Exam> exams = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Subject name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public Subject code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Set<Lesson> getLessons() {
        return lessons;
    }

    public Subject lessons(Set<Lesson> lessons) {
        this.lessons = lessons;
        return this;
    }

    public Subject addLesson(Lesson lesson) {
        this.lessons.add(lesson);
        lesson.setSubject(this);
        return this;
    }

    public Subject removeLesson(Lesson lesson) {
        this.lessons.remove(lesson);
        lesson.setSubject(null);
        return this;
    }

    public void setLessons(Set<Lesson> lessons) {
        this.lessons = lessons;
    }

    public Set<Exam> getExams() {
        return exams;
    }

    public Subject exams(Set<Exam> exams) {
        this.exams = exams;
        return this;
    }

    public Subject addExam(Exam exam) {
        this.exams.add(exam);
        exam.setSubject(this);
        return this;
    }

    public Subject removeExam(Exam exam) {
        this.exams.remove(exam);
        exam.setSubject(null);
        return this;
    }

    public void setExams(Set<Exam> exams) {
        this.exams = exams;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Subject)) {
            return false;
        }
        return id != null && id.equals(((Subject) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Subject{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            "}";
    }
}
