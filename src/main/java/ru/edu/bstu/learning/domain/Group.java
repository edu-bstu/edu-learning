package ru.edu.bstu.learning.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Группа
 */
@Entity
@Table(name = "jhi_group")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Group implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Название
     */
    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    /**
     * Курс
     */
    @NotNull
    @Min(value = 1)
    @Max(value = 5)
    @Column(name = "course", nullable = false)
    private Integer course;

    @OneToMany(mappedBy = "group")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Student> students = new HashSet<>();

    @OneToMany(mappedBy = "group")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ScheduleLesson> scheduleLessons = new HashSet<>();

    /**
     * Специальность группы
     */
    @ManyToOne
    @JsonIgnoreProperties(value = "groups", allowSetters = true)
    private Specialty specialty;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Group name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCourse() {
        return course;
    }

    public Group course(Integer course) {
        this.course = course;
        return this;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public Group students(Set<Student> students) {
        this.students = students;
        return this;
    }

    public Group addStudent(Student student) {
        this.students.add(student);
        student.setGroup(this);
        return this;
    }

    public Group removeStudent(Student student) {
        this.students.remove(student);
        student.setGroup(null);
        return this;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    public Set<ScheduleLesson> getScheduleLessons() {
        return scheduleLessons;
    }

    public Group scheduleLessons(Set<ScheduleLesson> scheduleLessons) {
        this.scheduleLessons = scheduleLessons;
        return this;
    }

    public Group addScheduleLesson(ScheduleLesson scheduleLesson) {
        this.scheduleLessons.add(scheduleLesson);
        scheduleLesson.setGroup(this);
        return this;
    }

    public Group removeScheduleLesson(ScheduleLesson scheduleLesson) {
        this.scheduleLessons.remove(scheduleLesson);
        scheduleLesson.setGroup(null);
        return this;
    }

    public void setScheduleLessons(Set<ScheduleLesson> scheduleLessons) {
        this.scheduleLessons = scheduleLessons;
    }

    public Specialty getSpecialty() {
        return specialty;
    }

    public Group specialty(Specialty specialty) {
        this.specialty = specialty;
        return this;
    }

    public void setSpecialty(Specialty specialty) {
        this.specialty = specialty;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Group)) {
            return false;
        }
        return id != null && id.equals(((Group) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Group{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", course=" + getCourse() +
            "}";
    }
}
