package ru.edu.bstu.learning.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import ru.edu.bstu.learning.domain.enumeration.MarkType;

/**
 * Оценка
 */
@Entity
@Table(name = "mark")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Mark implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Тип оченки
     */
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "mark_type", nullable = false)
    private MarkType markType;

    /**
     * Числовое значение оценки (если markType - SCORE)
     */
    @Column(name = "score")
    private Integer score;

    /**
     * Комментарий к оценке
     */
    @Column(name = "comment")
    private String comment;

    /**
     * Студент
     */
    @ManyToOne
    @JsonIgnoreProperties(value = "marks", allowSetters = true)
    private Student student;

    /**
     * Урок
     */
    @ManyToOne
    @JsonIgnoreProperties(value = "marks", allowSetters = true)
    private Lesson lesson;

    /**
     * Экзамен
     */
    @ManyToOne
    @JsonIgnoreProperties(value = "marks", allowSetters = true)
    private Exam exam;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MarkType getMarkType() {
        return markType;
    }

    public Mark markType(MarkType markType) {
        this.markType = markType;
        return this;
    }

    public void setMarkType(MarkType markType) {
        this.markType = markType;
    }

    public Integer getScore() {
        return score;
    }

    public Mark score(Integer score) {
        this.score = score;
        return this;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public Mark comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Student getStudent() {
        return student;
    }

    public Mark student(Student student) {
        this.student = student;
        return this;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public Mark lesson(Lesson lesson) {
        this.lesson = lesson;
        return this;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public Exam getExam() {
        return exam;
    }

    public Mark exam(Exam exam) {
        this.exam = exam;
        return this;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Mark)) {
            return false;
        }
        return id != null && id.equals(((Mark) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Mark{" +
            "id=" + getId() +
            ", markType='" + getMarkType() + "'" +
            ", score=" + getScore() +
            ", comment='" + getComment() + "'" +
            "}";
    }
}
