package ru.edu.bstu.learning.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * Сотрудник
 */
@Entity
@Table(name = "employee")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Дата трудоустройства
     */
    @Column(name = "employeement_at")
    private LocalDate employeementAt;

    /**
     * Общий стаж в годах
     */
    @Column(name = "total_experience")
    private Integer totalExperience;

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private Person person;

    @OneToMany(mappedBy = "teacher")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Lesson> lessons = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getEmployeementAt() {
        return employeementAt;
    }

    public Employee employeementAt(LocalDate employeementAt) {
        this.employeementAt = employeementAt;
        return this;
    }

    public void setEmployeementAt(LocalDate employeementAt) {
        this.employeementAt = employeementAt;
    }

    public Integer getTotalExperience() {
        return totalExperience;
    }

    public Employee totalExperience(Integer totalExperience) {
        this.totalExperience = totalExperience;
        return this;
    }

    public void setTotalExperience(Integer totalExperience) {
        this.totalExperience = totalExperience;
    }

    public Person getPerson() {
        return person;
    }

    public Employee person(Person person) {
        this.person = person;
        return this;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Set<Lesson> getLessons() {
        return lessons;
    }

    public Employee lessons(Set<Lesson> lessons) {
        this.lessons = lessons;
        return this;
    }

    public Employee addLesson(Lesson lesson) {
        this.lessons.add(lesson);
        lesson.setTeacher(this);
        return this;
    }

    public Employee removeLesson(Lesson lesson) {
        this.lessons.remove(lesson);
        lesson.setTeacher(null);
        return this;
    }

    public void setLessons(Set<Lesson> lessons) {
        this.lessons = lessons;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Employee)) {
            return false;
        }
        return id != null && id.equals(((Employee) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Employee{" +
            "id=" + getId() +
            ", employeementAt='" + getEmployeementAt() + "'" +
            ", totalExperience=" + getTotalExperience() +
            "}";
    }
}
