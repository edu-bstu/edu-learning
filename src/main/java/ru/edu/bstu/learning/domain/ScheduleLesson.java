package ru.edu.bstu.learning.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * Расписание урока
 */
@Entity
@Table(name = "schedule_lesson")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ScheduleLesson implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * Время
     */
    @NotNull
    @Column(name = "time", nullable = false)
    private Instant time;

    /**
     * День недели (1-6)
     */
    @NotNull
    @Min(value = 1)
    @Max(value = 6)
    @Column(name = "day", nullable = false)
    private Integer day;

    /**
     * Номер урока по порядку
     */
    @NotNull
    @Column(name = "number", nullable = false)
    private Integer number;

    /**
     * Числитель (true)/знаменатель (false)
     */
    @NotNull
    @Column(name = "numerator", nullable = false)
    private Boolean numerator;

    @OneToMany(mappedBy = "schedule")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Lesson> lessons = new HashSet<>();

    /**
     * Группа расписания
     */
    @ManyToOne
    @JsonIgnoreProperties(value = "scheduleLessons", allowSetters = true)
    private Group group;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getTime() {
        return time;
    }

    public ScheduleLesson time(Instant time) {
        this.time = time;
        return this;
    }

    public void setTime(Instant time) {
        this.time = time;
    }

    public Integer getDay() {
        return day;
    }

    public ScheduleLesson day(Integer day) {
        this.day = day;
        return this;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getNumber() {
        return number;
    }

    public ScheduleLesson number(Integer number) {
        this.number = number;
        return this;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Boolean isNumerator() {
        return numerator;
    }

    public ScheduleLesson numerator(Boolean numerator) {
        this.numerator = numerator;
        return this;
    }

    public void setNumerator(Boolean numerator) {
        this.numerator = numerator;
    }

    public Set<Lesson> getLessons() {
        return lessons;
    }

    public ScheduleLesson lessons(Set<Lesson> lessons) {
        this.lessons = lessons;
        return this;
    }

    public ScheduleLesson addLesson(Lesson lesson) {
        this.lessons.add(lesson);
        lesson.setSchedule(this);
        return this;
    }

    public ScheduleLesson removeLesson(Lesson lesson) {
        this.lessons.remove(lesson);
        lesson.setSchedule(null);
        return this;
    }

    public void setLessons(Set<Lesson> lessons) {
        this.lessons = lessons;
    }

    public Group getGroup() {
        return group;
    }

    public ScheduleLesson group(Group group) {
        this.group = group;
        return this;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ScheduleLesson)) {
            return false;
        }
        return id != null && id.equals(((ScheduleLesson) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ScheduleLesson{" +
            "id=" + getId() +
            ", time='" + getTime() + "'" +
            ", day=" + getDay() +
            ", number=" + getNumber() +
            ", numerator='" + isNumerator() + "'" +
            "}";
    }
}
