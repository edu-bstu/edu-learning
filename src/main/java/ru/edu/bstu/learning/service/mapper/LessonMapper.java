package ru.edu.bstu.learning.service.mapper;


import ru.edu.bstu.learning.domain.*;
import ru.edu.bstu.learning.service.dto.LessonDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Lesson} and its DTO {@link LessonDTO}.
 */
@Mapper(componentModel = "spring", uses = {SubjectMapper.class, EmployeeMapper.class, ScheduleLessonMapper.class})
public interface LessonMapper extends EntityMapper<LessonDTO, Lesson> {

    @Mapping(source = "subject.id", target = "subjectId")
    @Mapping(source = "teacher.id", target = "teacherId")
    @Mapping(source = "schedule.id", target = "scheduleId")
    LessonDTO toDto(Lesson lesson);

    @Mapping(target = "marks", ignore = true)
    @Mapping(target = "removeMark", ignore = true)
    @Mapping(source = "subjectId", target = "subject")
    @Mapping(source = "teacherId", target = "teacher")
    @Mapping(source = "scheduleId", target = "schedule")
    Lesson toEntity(LessonDTO lessonDTO);

    default Lesson fromId(Long id) {
        if (id == null) {
            return null;
        }
        Lesson lesson = new Lesson();
        lesson.setId(id);
        return lesson;
    }
}
