package ru.edu.bstu.learning.service;

import ru.edu.bstu.learning.service.dto.ScheduleLessonDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link ru.edu.bstu.learning.domain.ScheduleLesson}.
 */
public interface ScheduleLessonService {

    /**
     * Save a scheduleLesson.
     *
     * @param scheduleLessonDTO the entity to save.
     * @return the persisted entity.
     */
    ScheduleLessonDTO save(ScheduleLessonDTO scheduleLessonDTO);

    /**
     * Get all the scheduleLessons.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ScheduleLessonDTO> findAll(Pageable pageable);


    /**
     * Get the "id" scheduleLesson.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ScheduleLessonDTO> findOne(Long id);

    /**
     * Delete the "id" scheduleLesson.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
