package ru.edu.bstu.learning.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link ru.edu.bstu.learning.domain.Specialty} entity.
 */
@ApiModel(description = "Специальность")
public class SpecialtyDTO implements Serializable {
    
    private Long id;

    /**
     * Название
     */
    @NotNull
    @ApiModelProperty(value = "Название", required = true)
    private String name;

    /**
     * Код
     */
    @NotNull
    @ApiModelProperty(value = "Код", required = true)
    private String code;

    /**
     * Институт специальности
     */
    @ApiModelProperty(value = "Институт специальности")

    private Long institutionId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(Long institutionId) {
        this.institutionId = institutionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SpecialtyDTO)) {
            return false;
        }

        return id != null && id.equals(((SpecialtyDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SpecialtyDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            ", institutionId=" + getInstitutionId() +
            "}";
    }
}
