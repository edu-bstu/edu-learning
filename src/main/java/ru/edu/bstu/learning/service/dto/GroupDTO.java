package ru.edu.bstu.learning.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link ru.edu.bstu.learning.domain.Group} entity.
 */
@ApiModel(description = "Группа")
public class GroupDTO implements Serializable {
    
    private Long id;

    /**
     * Название
     */
    @NotNull
    @ApiModelProperty(value = "Название", required = true)
    private String name;

    /**
     * Курс
     */
    @NotNull
    @Min(value = 1)
    @Max(value = 5)
    @ApiModelProperty(value = "Курс", required = true)
    private Integer course;

    /**
     * Специальность группы
     */
    @ApiModelProperty(value = "Специальность группы")

    private Long specialtyId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public Long getSpecialtyId() {
        return specialtyId;
    }

    public void setSpecialtyId(Long specialtyId) {
        this.specialtyId = specialtyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GroupDTO)) {
            return false;
        }

        return id != null && id.equals(((GroupDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "GroupDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", course=" + getCourse() +
            ", specialtyId=" + getSpecialtyId() +
            "}";
    }
}
