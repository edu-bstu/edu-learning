package ru.edu.bstu.learning.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;
import ru.edu.bstu.learning.domain.enumeration.MarkType;

/**
 * A DTO for the {@link ru.edu.bstu.learning.domain.Mark} entity.
 */
@ApiModel(description = "Оценка")
public class MarkDTO implements Serializable {
    
    private Long id;

    /**
     * Тип оченки
     */
    @NotNull
    @ApiModelProperty(value = "Тип оченки", required = true)
    private MarkType markType;

    /**
     * Числовое значение оценки (если markType - SCORE)
     */
    @ApiModelProperty(value = "Числовое значение оценки (если markType - SCORE)")
    private Integer score;

    /**
     * Комментарий к оценке
     */
    @ApiModelProperty(value = "Комментарий к оценке")
    private String comment;

    /**
     * Студент
     */
    @ApiModelProperty(value = "Студент")

    private Long studentId;
    /**
     * Урок
     */
    @ApiModelProperty(value = "Урок")

    private Long lessonId;
    /**
     * Экзамен
     */
    @ApiModelProperty(value = "Экзамен")

    private Long examId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MarkType getMarkType() {
        return markType;
    }

    public void setMarkType(MarkType markType) {
        this.markType = markType;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getLessonId() {
        return lessonId;
    }

    public void setLessonId(Long lessonId) {
        this.lessonId = lessonId;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MarkDTO)) {
            return false;
        }

        return id != null && id.equals(((MarkDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MarkDTO{" +
            "id=" + getId() +
            ", markType='" + getMarkType() + "'" +
            ", score=" + getScore() +
            ", comment='" + getComment() + "'" +
            ", studentId=" + getStudentId() +
            ", lessonId=" + getLessonId() +
            ", examId=" + getExamId() +
            "}";
    }
}
