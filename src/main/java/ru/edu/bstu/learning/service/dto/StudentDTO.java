package ru.edu.bstu.learning.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link ru.edu.bstu.learning.domain.Student} entity.
 */
@ApiModel(description = "Студент")
public class StudentDTO implements Serializable {
    
    private Long id;

    /**
     * Номер зачетной книжки
     */
    @NotNull
    @ApiModelProperty(value = "Номер зачетной книжки", required = true)
    private String recordBookNumber;


    private Long personId;
    /**
     * Группа студента
     */
    @ApiModelProperty(value = "Группа студента")

    private Long groupId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecordBookNumber() {
        return recordBookNumber;
    }

    public void setRecordBookNumber(String recordBookNumber) {
        this.recordBookNumber = recordBookNumber;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StudentDTO)) {
            return false;
        }

        return id != null && id.equals(((StudentDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "StudentDTO{" +
            "id=" + getId() +
            ", recordBookNumber='" + getRecordBookNumber() + "'" +
            ", personId=" + getPersonId() +
            ", groupId=" + getGroupId() +
            "}";
    }
}
