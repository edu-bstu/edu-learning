package ru.edu.bstu.learning.service.mapper;


import ru.edu.bstu.learning.domain.*;
import ru.edu.bstu.learning.service.dto.ScheduleLessonDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ScheduleLesson} and its DTO {@link ScheduleLessonDTO}.
 */
@Mapper(componentModel = "spring", uses = {GroupMapper.class})
public interface ScheduleLessonMapper extends EntityMapper<ScheduleLessonDTO, ScheduleLesson> {

    @Mapping(source = "group.id", target = "groupId")
    ScheduleLessonDTO toDto(ScheduleLesson scheduleLesson);

    @Mapping(target = "lessons", ignore = true)
    @Mapping(target = "removeLesson", ignore = true)
    @Mapping(source = "groupId", target = "group")
    ScheduleLesson toEntity(ScheduleLessonDTO scheduleLessonDTO);

    default ScheduleLesson fromId(Long id) {
        if (id == null) {
            return null;
        }
        ScheduleLesson scheduleLesson = new ScheduleLesson();
        scheduleLesson.setId(id);
        return scheduleLesson;
    }
}
