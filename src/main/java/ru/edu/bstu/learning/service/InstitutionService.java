package ru.edu.bstu.learning.service;

import ru.edu.bstu.learning.service.dto.InstitutionDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link ru.edu.bstu.learning.domain.Institution}.
 */
public interface InstitutionService {

    /**
     * Save a institution.
     *
     * @param institutionDTO the entity to save.
     * @return the persisted entity.
     */
    InstitutionDTO save(InstitutionDTO institutionDTO);

    /**
     * Get all the institutions.
     *
     * @return the list of entities.
     */
    List<InstitutionDTO> findAll();


    /**
     * Get the "id" institution.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<InstitutionDTO> findOne(Long id);

    /**
     * Delete the "id" institution.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
