package ru.edu.bstu.learning.service;

import ru.edu.bstu.learning.service.dto.SpecialtyDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link ru.edu.bstu.learning.domain.Specialty}.
 */
public interface SpecialtyService {

    /**
     * Save a specialty.
     *
     * @param specialtyDTO the entity to save.
     * @return the persisted entity.
     */
    SpecialtyDTO save(SpecialtyDTO specialtyDTO);

    /**
     * Get all the specialties.
     *
     * @return the list of entities.
     */
    List<SpecialtyDTO> findAll();


    /**
     * Get the "id" specialty.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SpecialtyDTO> findOne(Long id);

    /**
     * Delete the "id" specialty.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
