package ru.edu.bstu.learning.service;

import ru.edu.bstu.learning.service.dto.MarkDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link ru.edu.bstu.learning.domain.Mark}.
 */
public interface MarkService {

    /**
     * Save a mark.
     *
     * @param markDTO the entity to save.
     * @return the persisted entity.
     */
    MarkDTO save(MarkDTO markDTO);

    /**
     * Get all the marks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MarkDTO> findAll(Pageable pageable);


    /**
     * Get the "id" mark.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MarkDTO> findOne(Long id);

    /**
     * Delete the "id" mark.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
