package ru.edu.bstu.learning.service.impl;

import ru.edu.bstu.learning.service.ScheduleLessonService;
import ru.edu.bstu.learning.domain.ScheduleLesson;
import ru.edu.bstu.learning.repository.ScheduleLessonRepository;
import ru.edu.bstu.learning.service.dto.ScheduleLessonDTO;
import ru.edu.bstu.learning.service.mapper.ScheduleLessonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ScheduleLesson}.
 */
@Service
@Transactional
public class ScheduleLessonServiceImpl implements ScheduleLessonService {

    private final Logger log = LoggerFactory.getLogger(ScheduleLessonServiceImpl.class);

    private final ScheduleLessonRepository scheduleLessonRepository;

    private final ScheduleLessonMapper scheduleLessonMapper;

    public ScheduleLessonServiceImpl(ScheduleLessonRepository scheduleLessonRepository, ScheduleLessonMapper scheduleLessonMapper) {
        this.scheduleLessonRepository = scheduleLessonRepository;
        this.scheduleLessonMapper = scheduleLessonMapper;
    }

    @Override
    public ScheduleLessonDTO save(ScheduleLessonDTO scheduleLessonDTO) {
        log.debug("Request to save ScheduleLesson : {}", scheduleLessonDTO);
        ScheduleLesson scheduleLesson = scheduleLessonMapper.toEntity(scheduleLessonDTO);
        scheduleLesson = scheduleLessonRepository.save(scheduleLesson);
        return scheduleLessonMapper.toDto(scheduleLesson);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ScheduleLessonDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ScheduleLessons");
        return scheduleLessonRepository.findAll(pageable)
            .map(scheduleLessonMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<ScheduleLessonDTO> findOne(Long id) {
        log.debug("Request to get ScheduleLesson : {}", id);
        return scheduleLessonRepository.findById(id)
            .map(scheduleLessonMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ScheduleLesson : {}", id);
        scheduleLessonRepository.deleteById(id);
    }
}
