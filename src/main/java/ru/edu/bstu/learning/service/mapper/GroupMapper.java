package ru.edu.bstu.learning.service.mapper;


import ru.edu.bstu.learning.domain.*;
import ru.edu.bstu.learning.service.dto.GroupDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Group} and its DTO {@link GroupDTO}.
 */
@Mapper(componentModel = "spring", uses = {SpecialtyMapper.class})
public interface GroupMapper extends EntityMapper<GroupDTO, Group> {

    @Mapping(source = "specialty.id", target = "specialtyId")
    GroupDTO toDto(Group group);

    @Mapping(target = "students", ignore = true)
    @Mapping(target = "removeStudent", ignore = true)
    @Mapping(target = "scheduleLessons", ignore = true)
    @Mapping(target = "removeScheduleLesson", ignore = true)
    @Mapping(source = "specialtyId", target = "specialty")
    Group toEntity(GroupDTO groupDTO);

    default Group fromId(Long id) {
        if (id == null) {
            return null;
        }
        Group group = new Group();
        group.setId(id);
        return group;
    }
}
