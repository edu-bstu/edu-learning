package ru.edu.bstu.learning.service.impl;

import ru.edu.bstu.learning.service.MarkService;
import ru.edu.bstu.learning.domain.Mark;
import ru.edu.bstu.learning.repository.MarkRepository;
import ru.edu.bstu.learning.service.dto.MarkDTO;
import ru.edu.bstu.learning.service.mapper.MarkMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Mark}.
 */
@Service
@Transactional
public class MarkServiceImpl implements MarkService {

    private final Logger log = LoggerFactory.getLogger(MarkServiceImpl.class);

    private final MarkRepository markRepository;

    private final MarkMapper markMapper;

    public MarkServiceImpl(MarkRepository markRepository, MarkMapper markMapper) {
        this.markRepository = markRepository;
        this.markMapper = markMapper;
    }

    @Override
    public MarkDTO save(MarkDTO markDTO) {
        log.debug("Request to save Mark : {}", markDTO);
        Mark mark = markMapper.toEntity(markDTO);
        mark = markRepository.save(mark);
        return markMapper.toDto(mark);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MarkDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Marks");
        return markRepository.findAll(pageable)
            .map(markMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<MarkDTO> findOne(Long id) {
        log.debug("Request to get Mark : {}", id);
        return markRepository.findById(id)
            .map(markMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Mark : {}", id);
        markRepository.deleteById(id);
    }
}
