package ru.edu.bstu.learning.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link ru.edu.bstu.learning.domain.ScheduleLesson} entity.
 */
@ApiModel(description = "Расписание урока")
public class ScheduleLessonDTO implements Serializable {
    
    private Long id;

    /**
     * Время
     */
    @NotNull
    @ApiModelProperty(value = "Время", required = true)
    private Instant time;

    /**
     * День недели (1-6)
     */
    @NotNull
    @Min(value = 1)
    @Max(value = 6)
    @ApiModelProperty(value = "День недели (1-6)", required = true)
    private Integer day;

    /**
     * Номер урока по порядку
     */
    @NotNull
    @ApiModelProperty(value = "Номер урока по порядку", required = true)
    private Integer number;

    /**
     * Числитель (true)/знаменатель (false)
     */
    @NotNull
    @ApiModelProperty(value = "Числитель (true)/знаменатель (false)", required = true)
    private Boolean numerator;

    /**
     * Группа расписания
     */
    @ApiModelProperty(value = "Группа расписания")

    private Long groupId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getTime() {
        return time;
    }

    public void setTime(Instant time) {
        this.time = time;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Boolean isNumerator() {
        return numerator;
    }

    public void setNumerator(Boolean numerator) {
        this.numerator = numerator;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ScheduleLessonDTO)) {
            return false;
        }

        return id != null && id.equals(((ScheduleLessonDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ScheduleLessonDTO{" +
            "id=" + getId() +
            ", time='" + getTime() + "'" +
            ", day=" + getDay() +
            ", number=" + getNumber() +
            ", numerator='" + isNumerator() + "'" +
            ", groupId=" + getGroupId() +
            "}";
    }
}
