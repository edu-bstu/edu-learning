package ru.edu.bstu.learning.service.mapper;


import ru.edu.bstu.learning.domain.*;
import ru.edu.bstu.learning.service.dto.SpecialtyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Specialty} and its DTO {@link SpecialtyDTO}.
 */
@Mapper(componentModel = "spring", uses = {InstitutionMapper.class})
public interface SpecialtyMapper extends EntityMapper<SpecialtyDTO, Specialty> {

    @Mapping(source = "institution.id", target = "institutionId")
    SpecialtyDTO toDto(Specialty specialty);

    @Mapping(target = "groups", ignore = true)
    @Mapping(target = "removeGroup", ignore = true)
    @Mapping(source = "institutionId", target = "institution")
    Specialty toEntity(SpecialtyDTO specialtyDTO);

    default Specialty fromId(Long id) {
        if (id == null) {
            return null;
        }
        Specialty specialty = new Specialty();
        specialty.setId(id);
        return specialty;
    }
}
