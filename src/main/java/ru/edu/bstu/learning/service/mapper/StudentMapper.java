package ru.edu.bstu.learning.service.mapper;


import ru.edu.bstu.learning.domain.*;
import ru.edu.bstu.learning.service.dto.StudentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Student} and its DTO {@link StudentDTO}.
 */
@Mapper(componentModel = "spring", uses = {PersonMapper.class, GroupMapper.class})
public interface StudentMapper extends EntityMapper<StudentDTO, Student> {

    @Mapping(source = "person.id", target = "personId")
    @Mapping(source = "group.id", target = "groupId")
    StudentDTO toDto(Student student);

    @Mapping(source = "personId", target = "person")
    @Mapping(target = "marks", ignore = true)
    @Mapping(target = "removeMark", ignore = true)
    @Mapping(source = "groupId", target = "group")
    Student toEntity(StudentDTO studentDTO);

    default Student fromId(Long id) {
        if (id == null) {
            return null;
        }
        Student student = new Student();
        student.setId(id);
        return student;
    }
}
