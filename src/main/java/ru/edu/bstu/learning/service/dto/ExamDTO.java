package ru.edu.bstu.learning.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link ru.edu.bstu.learning.domain.Exam} entity.
 */
@ApiModel(description = "Экзамен")
public class ExamDTO implements Serializable {
    
    private Long id;

    /**
     * Дата и время проведения экзамена
     */
    @NotNull
    @ApiModelProperty(value = "Дата и время проведения экзамена", required = true)
    private Instant dateTime;

    /**
     * Предмет по экзамену
     */
    @ApiModelProperty(value = "Предмет по экзамену")

    private Long subjectId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDateTime() {
        return dateTime;
    }

    public void setDateTime(Instant dateTime) {
        this.dateTime = dateTime;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExamDTO)) {
            return false;
        }

        return id != null && id.equals(((ExamDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExamDTO{" +
            "id=" + getId() +
            ", dateTime='" + getDateTime() + "'" +
            ", subjectId=" + getSubjectId() +
            "}";
    }
}
