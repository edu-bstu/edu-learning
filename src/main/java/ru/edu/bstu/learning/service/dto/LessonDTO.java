package ru.edu.bstu.learning.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link ru.edu.bstu.learning.domain.Lesson} entity.
 */
@ApiModel(description = "Урок")
public class LessonDTO implements Serializable {
    
    private Long id;

    /**
     * Дата урока
     */
    @NotNull
    @ApiModelProperty(value = "Дата урока", required = true)
    private LocalDate date;

    /**
     * Предмет урока
     */
    @ApiModelProperty(value = "Предмет урока")

    private Long subjectId;
    /**
     * Преподаватель урока
     */
    @ApiModelProperty(value = "Преподаватель урока")

    private Long teacherId;
    /**
     * Расписание урока
     */
    @ApiModelProperty(value = "Расписание урока")

    private Long scheduleId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long employeeId) {
        this.teacherId = employeeId;
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleLessonId) {
        this.scheduleId = scheduleLessonId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LessonDTO)) {
            return false;
        }

        return id != null && id.equals(((LessonDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LessonDTO{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", subjectId=" + getSubjectId() +
            ", teacherId=" + getTeacherId() +
            ", scheduleId=" + getScheduleId() +
            "}";
    }
}
