package ru.edu.bstu.learning.service.mapper;


import ru.edu.bstu.learning.domain.*;
import ru.edu.bstu.learning.service.dto.MarkDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Mark} and its DTO {@link MarkDTO}.
 */
@Mapper(componentModel = "spring", uses = {StudentMapper.class, LessonMapper.class, ExamMapper.class})
public interface MarkMapper extends EntityMapper<MarkDTO, Mark> {

    @Mapping(source = "student.id", target = "studentId")
    @Mapping(source = "lesson.id", target = "lessonId")
    @Mapping(source = "exam.id", target = "examId")
    MarkDTO toDto(Mark mark);

    @Mapping(source = "studentId", target = "student")
    @Mapping(source = "lessonId", target = "lesson")
    @Mapping(source = "examId", target = "exam")
    Mark toEntity(MarkDTO markDTO);

    default Mark fromId(Long id) {
        if (id == null) {
            return null;
        }
        Mark mark = new Mark();
        mark.setId(id);
        return mark;
    }
}
