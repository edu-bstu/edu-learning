package ru.edu.bstu.learning.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link ru.edu.bstu.learning.domain.Person} entity.
 */
@ApiModel(description = "Персона")
public class PersonDTO implements Serializable {
    
    private Long id;

    /**
     * Фамилия
     */
    @NotNull
    @ApiModelProperty(value = "Фамилия", required = true)
    private String surname;

    /**
     * Имя
     */
    @NotNull
    @ApiModelProperty(value = "Имя", required = true)
    private String name;

    /**
     * Отчество
     */
    @ApiModelProperty(value = "Отчество")
    private String patronymic;

    /**
     * Дата рождения
     */
    @NotNull
    @ApiModelProperty(value = "Дата рождения", required = true)
    private LocalDate dateOfBirth;

    /**
     * Контактная почта
     */
    @ApiModelProperty(value = "Контактная почта")
    private String email;

    /**
     * Контактный номер телефона
     */
    @ApiModelProperty(value = "Контактный номер телефона")
    private String phoneNumber;


    private Long userId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PersonDTO)) {
            return false;
        }

        return id != null && id.equals(((PersonDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PersonDTO{" +
            "id=" + getId() +
            ", surname='" + getSurname() + "'" +
            ", name='" + getName() + "'" +
            ", patronymic='" + getPatronymic() + "'" +
            ", dateOfBirth='" + getDateOfBirth() + "'" +
            ", email='" + getEmail() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", userId=" + getUserId() +
            "}";
    }
}
