package ru.edu.bstu.learning.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link ru.edu.bstu.learning.domain.Employee} entity.
 */
@ApiModel(description = "Сотрудник")
public class EmployeeDTO implements Serializable {
    
    private Long id;

    /**
     * Дата трудоустройства
     */
    @ApiModelProperty(value = "Дата трудоустройства")
    private LocalDate employeementAt;

    /**
     * Общий стаж в годах
     */
    @ApiModelProperty(value = "Общий стаж в годах")
    private Integer totalExperience;


    private Long personId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getEmployeementAt() {
        return employeementAt;
    }

    public void setEmployeementAt(LocalDate employeementAt) {
        this.employeementAt = employeementAt;
    }

    public Integer getTotalExperience() {
        return totalExperience;
    }

    public void setTotalExperience(Integer totalExperience) {
        this.totalExperience = totalExperience;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EmployeeDTO)) {
            return false;
        }

        return id != null && id.equals(((EmployeeDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EmployeeDTO{" +
            "id=" + getId() +
            ", employeementAt='" + getEmployeementAt() + "'" +
            ", totalExperience=" + getTotalExperience() +
            ", personId=" + getPersonId() +
            "}";
    }
}
