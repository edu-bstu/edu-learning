package ru.edu.bstu.learning.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link ru.edu.bstu.learning.domain.Subject} entity.
 */
@ApiModel(description = "Изучаемый предмет")
public class SubjectDTO implements Serializable {
    
    private Long id;

    /**
     * Название
     */
    @NotNull
    @ApiModelProperty(value = "Название", required = true)
    private String name;

    /**
     * Код
     */
    @ApiModelProperty(value = "Код")
    private String code;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SubjectDTO)) {
            return false;
        }

        return id != null && id.equals(((SubjectDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SubjectDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            "}";
    }
}
