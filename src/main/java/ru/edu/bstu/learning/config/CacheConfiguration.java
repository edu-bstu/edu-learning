package ru.edu.bstu.learning.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import org.hibernate.cache.jcache.ConfigSettings;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.info.GitProperties;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import io.github.jhipster.config.cache.PrefixedKeyGenerator;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {
    private GitProperties gitProperties;
    private BuildProperties buildProperties;
    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache = jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, ru.edu.bstu.learning.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, ru.edu.bstu.learning.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, ru.edu.bstu.learning.domain.User.class.getName());
            createCache(cm, ru.edu.bstu.learning.domain.Authority.class.getName());
            createCache(cm, ru.edu.bstu.learning.domain.User.class.getName() + ".authorities");
            createCache(cm, ru.edu.bstu.learning.domain.Subject.class.getName());
            createCache(cm, ru.edu.bstu.learning.domain.Subject.class.getName() + ".lessons");
            createCache(cm, ru.edu.bstu.learning.domain.Subject.class.getName() + ".exams");
            createCache(cm, ru.edu.bstu.learning.domain.Lesson.class.getName());
            createCache(cm, ru.edu.bstu.learning.domain.Lesson.class.getName() + ".marks");
            createCache(cm, ru.edu.bstu.learning.domain.ScheduleLesson.class.getName());
            createCache(cm, ru.edu.bstu.learning.domain.ScheduleLesson.class.getName() + ".lessons");
            createCache(cm, ru.edu.bstu.learning.domain.Mark.class.getName());
            createCache(cm, ru.edu.bstu.learning.domain.Exam.class.getName());
            createCache(cm, ru.edu.bstu.learning.domain.Exam.class.getName() + ".marks");
            createCache(cm, ru.edu.bstu.learning.domain.Person.class.getName());
            createCache(cm, ru.edu.bstu.learning.domain.Student.class.getName());
            createCache(cm, ru.edu.bstu.learning.domain.Student.class.getName() + ".marks");
            createCache(cm, ru.edu.bstu.learning.domain.Employee.class.getName());
            createCache(cm, ru.edu.bstu.learning.domain.Employee.class.getName() + ".lessons");
            createCache(cm, ru.edu.bstu.learning.domain.Institution.class.getName());
            createCache(cm, ru.edu.bstu.learning.domain.Institution.class.getName() + ".specialties");
            createCache(cm, ru.edu.bstu.learning.domain.Specialty.class.getName());
            createCache(cm, ru.edu.bstu.learning.domain.Specialty.class.getName() + ".groups");
            createCache(cm, ru.edu.bstu.learning.domain.Group.class.getName());
            createCache(cm, ru.edu.bstu.learning.domain.Group.class.getName() + ".students");
            createCache(cm, ru.edu.bstu.learning.domain.Group.class.getName() + ".scheduleLessons");
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache == null) {
            cm.createCache(cacheName, jcacheConfiguration);
        }
    }

    @Autowired(required = false)
    public void setGitProperties(GitProperties gitProperties) {
        this.gitProperties = gitProperties;
    }

    @Autowired(required = false)
    public void setBuildProperties(BuildProperties buildProperties) {
        this.buildProperties = buildProperties;
    }

    @Bean
    public KeyGenerator keyGenerator() {
        return new PrefixedKeyGenerator(this.gitProperties, this.buildProperties);
    }
}
