import { IStudent } from 'app/shared/model/student.model';
import { IScheduleLesson } from 'app/shared/model/schedule-lesson.model';

export interface IGroup {
  id?: number;
  name?: string;
  course?: number;
  students?: IStudent[];
  scheduleLessons?: IScheduleLesson[];
  specialtyId?: number;
}

export const defaultValue: Readonly<IGroup> = {};
