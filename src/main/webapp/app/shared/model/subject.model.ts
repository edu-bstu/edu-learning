import { ILesson } from 'app/shared/model/lesson.model';
import { IExam } from 'app/shared/model/exam.model';

export interface ISubject {
  id?: number;
  name?: string;
  code?: string;
  lessons?: ILesson[];
  exams?: IExam[];
}

export const defaultValue: Readonly<ISubject> = {};
