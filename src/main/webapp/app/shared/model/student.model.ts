import { IMark } from 'app/shared/model/mark.model';

export interface IStudent {
  id?: number;
  recordBookNumber?: string;
  personId?: number;
  marks?: IMark[];
  groupId?: number;
}

export const defaultValue: Readonly<IStudent> = {};
