import { Moment } from 'moment';
import { IMark } from 'app/shared/model/mark.model';

export interface IExam {
  id?: number;
  dateTime?: string;
  marks?: IMark[];
  subjectId?: number;
}

export const defaultValue: Readonly<IExam> = {};
