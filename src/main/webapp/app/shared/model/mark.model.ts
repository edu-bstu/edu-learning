import { MarkType } from 'app/shared/model/enumerations/mark-type.model';

export interface IMark {
  id?: number;
  markType?: MarkType;
  score?: number;
  comment?: string;
  studentId?: number;
  lessonId?: number;
  examId?: number;
}

export const defaultValue: Readonly<IMark> = {};
