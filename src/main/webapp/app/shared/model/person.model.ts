import { Moment } from 'moment';

export interface IPerson {
  id?: number;
  surname?: string;
  name?: string;
  patronymic?: string;
  dateOfBirth?: string;
  email?: string;
  phoneNumber?: string;
  userId?: number;
}

export const defaultValue: Readonly<IPerson> = {};
