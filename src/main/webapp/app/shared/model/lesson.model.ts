import { Moment } from 'moment';
import { IMark } from 'app/shared/model/mark.model';

export interface ILesson {
  id?: number;
  date?: string;
  marks?: IMark[];
  subjectId?: number;
  teacherId?: number;
  scheduleId?: number;
}

export const defaultValue: Readonly<ILesson> = {};
