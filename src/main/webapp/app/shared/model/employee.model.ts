import { Moment } from 'moment';
import { ILesson } from 'app/shared/model/lesson.model';

export interface IEmployee {
  id?: number;
  employeementAt?: string;
  totalExperience?: number;
  personId?: number;
  lessons?: ILesson[];
}

export const defaultValue: Readonly<IEmployee> = {};
