import { Moment } from 'moment';
import { ILesson } from 'app/shared/model/lesson.model';

export interface IScheduleLesson {
  id?: number;
  time?: string;
  day?: number;
  number?: number;
  numerator?: boolean;
  lessons?: ILesson[];
  groupId?: number;
}

export const defaultValue: Readonly<IScheduleLesson> = {
  numerator: false,
};
