import { ISpecialty } from 'app/shared/model/specialty.model';

export interface IInstitution {
  id?: number;
  name?: string;
  code?: string;
  specialties?: ISpecialty[];
}

export const defaultValue: Readonly<IInstitution> = {};
