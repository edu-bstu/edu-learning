import { IGroup } from 'app/shared/model/group.model';

export interface ISpecialty {
  id?: number;
  name?: string;
  code?: string;
  groups?: IGroup[];
  institutionId?: number;
}

export const defaultValue: Readonly<ISpecialty> = {};
