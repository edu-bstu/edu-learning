export const enum MarkType {
  SCORE = 'SCORE',

  ABSENCE = 'ABSENCE',

  OFFSET = 'OFFSET',

  NEGLECT = 'NEGLECT',
}
