import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import locale, { LocaleState } from './locale';
import authentication, { AuthenticationState } from './authentication';
import applicationProfile, { ApplicationProfileState } from './application-profile';

import administration, { AdministrationState } from 'app/modules/administration/administration.reducer';
import userManagement, { UserManagementState } from 'app/modules/administration/user-management/user-management.reducer';
import register, { RegisterState } from 'app/modules/account/register/register.reducer';
import activate, { ActivateState } from 'app/modules/account/activate/activate.reducer';
import password, { PasswordState } from 'app/modules/account/password/password.reducer';
import settings, { SettingsState } from 'app/modules/account/settings/settings.reducer';
import passwordReset, { PasswordResetState } from 'app/modules/account/password-reset/password-reset.reducer';
// prettier-ignore
import subject, {
  SubjectState
} from 'app/entities/subject/subject.reducer';
// prettier-ignore
import lesson, {
  LessonState
} from 'app/entities/lesson/lesson.reducer';
// prettier-ignore
import scheduleLesson, {
  ScheduleLessonState
} from 'app/entities/schedule-lesson/schedule-lesson.reducer';
// prettier-ignore
import mark, {
  MarkState
} from 'app/entities/mark/mark.reducer';
// prettier-ignore
import exam, {
  ExamState
} from 'app/entities/exam/exam.reducer';
// prettier-ignore
import person, {
  PersonState
} from 'app/entities/person/person.reducer';
// prettier-ignore
import student, {
  StudentState
} from 'app/entities/student/student.reducer';
// prettier-ignore
import employee, {
  EmployeeState
} from 'app/entities/employee/employee.reducer';
// prettier-ignore
import institution, {
  InstitutionState
} from 'app/entities/institution/institution.reducer';
// prettier-ignore
import specialty, {
  SpecialtyState
} from 'app/entities/specialty/specialty.reducer';
// prettier-ignore
import group, {
  GroupState
} from 'app/entities/group/group.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly locale: LocaleState;
  readonly applicationProfile: ApplicationProfileState;
  readonly administration: AdministrationState;
  readonly userManagement: UserManagementState;
  readonly register: RegisterState;
  readonly activate: ActivateState;
  readonly passwordReset: PasswordResetState;
  readonly password: PasswordState;
  readonly settings: SettingsState;
  readonly subject: SubjectState;
  readonly lesson: LessonState;
  readonly scheduleLesson: ScheduleLessonState;
  readonly mark: MarkState;
  readonly exam: ExamState;
  readonly person: PersonState;
  readonly student: StudentState;
  readonly employee: EmployeeState;
  readonly institution: InstitutionState;
  readonly specialty: SpecialtyState;
  readonly group: GroupState;
  /* jhipster-needle-add-reducer-type - JHipster will add reducer type here */
  readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  locale,
  applicationProfile,
  administration,
  userManagement,
  register,
  activate,
  passwordReset,
  password,
  settings,
  subject,
  lesson,
  scheduleLesson,
  mark,
  exam,
  person,
  student,
  employee,
  institution,
  specialty,
  group,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar,
});

export default rootReducer;
