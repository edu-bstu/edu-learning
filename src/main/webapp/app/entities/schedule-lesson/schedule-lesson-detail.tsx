import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './schedule-lesson.reducer';
import { IScheduleLesson } from 'app/shared/model/schedule-lesson.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IScheduleLessonDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ScheduleLessonDetail = (props: IScheduleLessonDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { scheduleLessonEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="learningApp.scheduleLesson.detail.title">ScheduleLesson</Translate> [<b>{scheduleLessonEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="time">
              <Translate contentKey="learningApp.scheduleLesson.time">Time</Translate>
            </span>
            <UncontrolledTooltip target="time">
              <Translate contentKey="learningApp.scheduleLesson.help.time" />
            </UncontrolledTooltip>
          </dt>
          <dd>
            {scheduleLessonEntity.time ? <TextFormat value={scheduleLessonEntity.time} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="day">
              <Translate contentKey="learningApp.scheduleLesson.day">Day</Translate>
            </span>
            <UncontrolledTooltip target="day">
              <Translate contentKey="learningApp.scheduleLesson.help.day" />
            </UncontrolledTooltip>
          </dt>
          <dd>{scheduleLessonEntity.day}</dd>
          <dt>
            <span id="number">
              <Translate contentKey="learningApp.scheduleLesson.number">Number</Translate>
            </span>
            <UncontrolledTooltip target="number">
              <Translate contentKey="learningApp.scheduleLesson.help.number" />
            </UncontrolledTooltip>
          </dt>
          <dd>{scheduleLessonEntity.number}</dd>
          <dt>
            <span id="numerator">
              <Translate contentKey="learningApp.scheduleLesson.numerator">Numerator</Translate>
            </span>
            <UncontrolledTooltip target="numerator">
              <Translate contentKey="learningApp.scheduleLesson.help.numerator" />
            </UncontrolledTooltip>
          </dt>
          <dd>{scheduleLessonEntity.numerator ? 'true' : 'false'}</dd>
          <dt>
            <Translate contentKey="learningApp.scheduleLesson.group">Group</Translate>
          </dt>
          <dd>{scheduleLessonEntity.groupId ? scheduleLessonEntity.groupId : ''}</dd>
        </dl>
        <Button tag={Link} to="/schedule-lesson" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/schedule-lesson/${scheduleLessonEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ scheduleLesson }: IRootState) => ({
  scheduleLessonEntity: scheduleLesson.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ScheduleLessonDetail);
