import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ScheduleLesson from './schedule-lesson';
import ScheduleLessonDetail from './schedule-lesson-detail';
import ScheduleLessonUpdate from './schedule-lesson-update';
import ScheduleLessonDeleteDialog from './schedule-lesson-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ScheduleLessonUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ScheduleLessonUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ScheduleLessonDetail} />
      <ErrorBoundaryRoute path={match.url} component={ScheduleLesson} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ScheduleLessonDeleteDialog} />
  </>
);

export default Routes;
