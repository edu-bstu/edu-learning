import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IGroup } from 'app/shared/model/group.model';
import { getEntities as getGroups } from 'app/entities/group/group.reducer';
import { getEntity, updateEntity, createEntity, reset } from './schedule-lesson.reducer';
import { IScheduleLesson } from 'app/shared/model/schedule-lesson.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IScheduleLessonUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ScheduleLessonUpdate = (props: IScheduleLessonUpdateProps) => {
  const [groupId, setGroupId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { scheduleLessonEntity, groups, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/schedule-lesson' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getGroups();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.time = convertDateTimeToServer(values.time);

    if (errors.length === 0) {
      const entity = {
        ...scheduleLessonEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="learningApp.scheduleLesson.home.createOrEditLabel">
            <Translate contentKey="learningApp.scheduleLesson.home.createOrEditLabel">Create or edit a ScheduleLesson</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : scheduleLessonEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="schedule-lesson-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="schedule-lesson-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="timeLabel" for="schedule-lesson-time">
                  <Translate contentKey="learningApp.scheduleLesson.time">Time</Translate>
                </Label>
                <AvInput
                  id="schedule-lesson-time"
                  type="datetime-local"
                  className="form-control"
                  name="time"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.scheduleLessonEntity.time)}
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
                <UncontrolledTooltip target="timeLabel">
                  <Translate contentKey="learningApp.scheduleLesson.help.time" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="dayLabel" for="schedule-lesson-day">
                  <Translate contentKey="learningApp.scheduleLesson.day">Day</Translate>
                </Label>
                <AvField
                  id="schedule-lesson-day"
                  type="string"
                  className="form-control"
                  name="day"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    min: { value: 1, errorMessage: translate('entity.validation.min', { min: 1 }) },
                    max: { value: 6, errorMessage: translate('entity.validation.max', { max: 6 }) },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
                <UncontrolledTooltip target="dayLabel">
                  <Translate contentKey="learningApp.scheduleLesson.help.day" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="numberLabel" for="schedule-lesson-number">
                  <Translate contentKey="learningApp.scheduleLesson.number">Number</Translate>
                </Label>
                <AvField
                  id="schedule-lesson-number"
                  type="string"
                  className="form-control"
                  name="number"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    number: { value: true, errorMessage: translate('entity.validation.number') },
                  }}
                />
                <UncontrolledTooltip target="numberLabel">
                  <Translate contentKey="learningApp.scheduleLesson.help.number" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup check>
                <Label id="numeratorLabel">
                  <AvInput id="schedule-lesson-numerator" type="checkbox" className="form-check-input" name="numerator" />
                  <Translate contentKey="learningApp.scheduleLesson.numerator">Numerator</Translate>
                </Label>
                <UncontrolledTooltip target="numeratorLabel">
                  <Translate contentKey="learningApp.scheduleLesson.help.numerator" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label for="schedule-lesson-group">
                  <Translate contentKey="learningApp.scheduleLesson.group">Group</Translate>
                </Label>
                <AvInput id="schedule-lesson-group" type="select" className="form-control" name="groupId">
                  <option value="" key="0" />
                  {groups
                    ? groups.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/schedule-lesson" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  groups: storeState.group.entities,
  scheduleLessonEntity: storeState.scheduleLesson.entity,
  loading: storeState.scheduleLesson.loading,
  updating: storeState.scheduleLesson.updating,
  updateSuccess: storeState.scheduleLesson.updateSuccess,
});

const mapDispatchToProps = {
  getGroups,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ScheduleLessonUpdate);
