import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IScheduleLesson, defaultValue } from 'app/shared/model/schedule-lesson.model';

export const ACTION_TYPES = {
  FETCH_SCHEDULELESSON_LIST: 'scheduleLesson/FETCH_SCHEDULELESSON_LIST',
  FETCH_SCHEDULELESSON: 'scheduleLesson/FETCH_SCHEDULELESSON',
  CREATE_SCHEDULELESSON: 'scheduleLesson/CREATE_SCHEDULELESSON',
  UPDATE_SCHEDULELESSON: 'scheduleLesson/UPDATE_SCHEDULELESSON',
  DELETE_SCHEDULELESSON: 'scheduleLesson/DELETE_SCHEDULELESSON',
  RESET: 'scheduleLesson/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IScheduleLesson>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type ScheduleLessonState = Readonly<typeof initialState>;

// Reducer

export default (state: ScheduleLessonState = initialState, action): ScheduleLessonState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_SCHEDULELESSON_LIST):
    case REQUEST(ACTION_TYPES.FETCH_SCHEDULELESSON):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_SCHEDULELESSON):
    case REQUEST(ACTION_TYPES.UPDATE_SCHEDULELESSON):
    case REQUEST(ACTION_TYPES.DELETE_SCHEDULELESSON):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_SCHEDULELESSON_LIST):
    case FAILURE(ACTION_TYPES.FETCH_SCHEDULELESSON):
    case FAILURE(ACTION_TYPES.CREATE_SCHEDULELESSON):
    case FAILURE(ACTION_TYPES.UPDATE_SCHEDULELESSON):
    case FAILURE(ACTION_TYPES.DELETE_SCHEDULELESSON):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_SCHEDULELESSON_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_SCHEDULELESSON):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_SCHEDULELESSON):
    case SUCCESS(ACTION_TYPES.UPDATE_SCHEDULELESSON):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_SCHEDULELESSON):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/schedule-lessons';

// Actions

export const getEntities: ICrudGetAllAction<IScheduleLesson> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_SCHEDULELESSON_LIST,
    payload: axios.get<IScheduleLesson>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IScheduleLesson> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_SCHEDULELESSON,
    payload: axios.get<IScheduleLesson>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IScheduleLesson> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_SCHEDULELESSON,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IScheduleLesson> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_SCHEDULELESSON,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IScheduleLesson> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_SCHEDULELESSON,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
