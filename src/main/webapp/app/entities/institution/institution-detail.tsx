import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './institution.reducer';
import { IInstitution } from 'app/shared/model/institution.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IInstitutionDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const InstitutionDetail = (props: IInstitutionDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { institutionEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="learningApp.institution.detail.title">Institution</Translate> [<b>{institutionEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="name">
              <Translate contentKey="learningApp.institution.name">Name</Translate>
            </span>
            <UncontrolledTooltip target="name">
              <Translate contentKey="learningApp.institution.help.name" />
            </UncontrolledTooltip>
          </dt>
          <dd>{institutionEntity.name}</dd>
          <dt>
            <span id="code">
              <Translate contentKey="learningApp.institution.code">Code</Translate>
            </span>
            <UncontrolledTooltip target="code">
              <Translate contentKey="learningApp.institution.help.code" />
            </UncontrolledTooltip>
          </dt>
          <dd>{institutionEntity.code}</dd>
        </dl>
        <Button tag={Link} to="/institution" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/institution/${institutionEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ institution }: IRootState) => ({
  institutionEntity: institution.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(InstitutionDetail);
