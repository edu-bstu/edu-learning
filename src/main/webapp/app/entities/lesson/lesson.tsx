import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './lesson.reducer';
import { ILesson } from 'app/shared/model/lesson.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ILessonProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Lesson = (props: ILessonProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { lessonList, match, loading } = props;
  return (
    <div>
      <h2 id="lesson-heading">
        <Translate contentKey="learningApp.lesson.home.title">Lessons</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp;
          <Translate contentKey="learningApp.lesson.home.createLabel">Create new Lesson</Translate>
        </Link>
      </h2>
      <div className="table-responsive">
        {lessonList && lessonList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="learningApp.lesson.date">Date</Translate>
                </th>
                <th>
                  <Translate contentKey="learningApp.lesson.subject">Subject</Translate>
                </th>
                <th>
                  <Translate contentKey="learningApp.lesson.teacher">Teacher</Translate>
                </th>
                <th>
                  <Translate contentKey="learningApp.lesson.schedule">Schedule</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {lessonList.map((lesson, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${lesson.id}`} color="link" size="sm">
                      {lesson.id}
                    </Button>
                  </td>
                  <td>{lesson.date ? <TextFormat type="date" value={lesson.date} format={APP_LOCAL_DATE_FORMAT} /> : null}</td>
                  <td>{lesson.subjectId ? <Link to={`subject/${lesson.subjectId}`}>{lesson.subjectId}</Link> : ''}</td>
                  <td>{lesson.teacherId ? <Link to={`employee/${lesson.teacherId}`}>{lesson.teacherId}</Link> : ''}</td>
                  <td>{lesson.scheduleId ? <Link to={`schedule-lesson/${lesson.scheduleId}`}>{lesson.scheduleId}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${lesson.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${lesson.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${lesson.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="learningApp.lesson.home.notFound">No Lessons found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ lesson }: IRootState) => ({
  lessonList: lesson.entities,
  loading: lesson.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Lesson);
