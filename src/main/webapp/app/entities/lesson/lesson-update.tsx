import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { ISubject } from 'app/shared/model/subject.model';
import { getEntities as getSubjects } from 'app/entities/subject/subject.reducer';
import { IEmployee } from 'app/shared/model/employee.model';
import { getEntities as getEmployees } from 'app/entities/employee/employee.reducer';
import { IScheduleLesson } from 'app/shared/model/schedule-lesson.model';
import { getEntities as getScheduleLessons } from 'app/entities/schedule-lesson/schedule-lesson.reducer';
import { getEntity, updateEntity, createEntity, reset } from './lesson.reducer';
import { ILesson } from 'app/shared/model/lesson.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ILessonUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const LessonUpdate = (props: ILessonUpdateProps) => {
  const [subjectId, setSubjectId] = useState('0');
  const [teacherId, setTeacherId] = useState('0');
  const [scheduleId, setScheduleId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { lessonEntity, subjects, employees, scheduleLessons, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/lesson');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getSubjects();
    props.getEmployees();
    props.getScheduleLessons();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...lessonEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="learningApp.lesson.home.createOrEditLabel">
            <Translate contentKey="learningApp.lesson.home.createOrEditLabel">Create or edit a Lesson</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : lessonEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="lesson-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="lesson-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="dateLabel" for="lesson-date">
                  <Translate contentKey="learningApp.lesson.date">Date</Translate>
                </Label>
                <AvField
                  id="lesson-date"
                  type="date"
                  className="form-control"
                  name="date"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
                <UncontrolledTooltip target="dateLabel">
                  <Translate contentKey="learningApp.lesson.help.date" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label for="lesson-subject">
                  <Translate contentKey="learningApp.lesson.subject">Subject</Translate>
                </Label>
                <AvInput id="lesson-subject" type="select" className="form-control" name="subjectId">
                  <option value="" key="0" />
                  {subjects
                    ? subjects.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="lesson-teacher">
                  <Translate contentKey="learningApp.lesson.teacher">Teacher</Translate>
                </Label>
                <AvInput id="lesson-teacher" type="select" className="form-control" name="teacherId">
                  <option value="" key="0" />
                  {employees
                    ? employees.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="lesson-schedule">
                  <Translate contentKey="learningApp.lesson.schedule">Schedule</Translate>
                </Label>
                <AvInput id="lesson-schedule" type="select" className="form-control" name="scheduleId">
                  <option value="" key="0" />
                  {scheduleLessons
                    ? scheduleLessons.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/lesson" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  subjects: storeState.subject.entities,
  employees: storeState.employee.entities,
  scheduleLessons: storeState.scheduleLesson.entities,
  lessonEntity: storeState.lesson.entity,
  loading: storeState.lesson.loading,
  updating: storeState.lesson.updating,
  updateSuccess: storeState.lesson.updateSuccess,
});

const mapDispatchToProps = {
  getSubjects,
  getEmployees,
  getScheduleLessons,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(LessonUpdate);
