import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './lesson.reducer';
import { ILesson } from 'app/shared/model/lesson.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ILessonDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const LessonDetail = (props: ILessonDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { lessonEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="learningApp.lesson.detail.title">Lesson</Translate> [<b>{lessonEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="date">
              <Translate contentKey="learningApp.lesson.date">Date</Translate>
            </span>
            <UncontrolledTooltip target="date">
              <Translate contentKey="learningApp.lesson.help.date" />
            </UncontrolledTooltip>
          </dt>
          <dd>{lessonEntity.date ? <TextFormat value={lessonEntity.date} type="date" format={APP_LOCAL_DATE_FORMAT} /> : null}</dd>
          <dt>
            <Translate contentKey="learningApp.lesson.subject">Subject</Translate>
          </dt>
          <dd>{lessonEntity.subjectId ? lessonEntity.subjectId : ''}</dd>
          <dt>
            <Translate contentKey="learningApp.lesson.teacher">Teacher</Translate>
          </dt>
          <dd>{lessonEntity.teacherId ? lessonEntity.teacherId : ''}</dd>
          <dt>
            <Translate contentKey="learningApp.lesson.schedule">Schedule</Translate>
          </dt>
          <dd>{lessonEntity.scheduleId ? lessonEntity.scheduleId : ''}</dd>
        </dl>
        <Button tag={Link} to="/lesson" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/lesson/${lessonEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ lesson }: IRootState) => ({
  lessonEntity: lesson.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(LessonDetail);
