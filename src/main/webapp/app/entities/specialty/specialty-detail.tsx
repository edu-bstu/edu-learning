import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './specialty.reducer';
import { ISpecialty } from 'app/shared/model/specialty.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ISpecialtyDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const SpecialtyDetail = (props: ISpecialtyDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { specialtyEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="learningApp.specialty.detail.title">Specialty</Translate> [<b>{specialtyEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="name">
              <Translate contentKey="learningApp.specialty.name">Name</Translate>
            </span>
            <UncontrolledTooltip target="name">
              <Translate contentKey="learningApp.specialty.help.name" />
            </UncontrolledTooltip>
          </dt>
          <dd>{specialtyEntity.name}</dd>
          <dt>
            <span id="code">
              <Translate contentKey="learningApp.specialty.code">Code</Translate>
            </span>
            <UncontrolledTooltip target="code">
              <Translate contentKey="learningApp.specialty.help.code" />
            </UncontrolledTooltip>
          </dt>
          <dd>{specialtyEntity.code}</dd>
          <dt>
            <Translate contentKey="learningApp.specialty.institution">Institution</Translate>
          </dt>
          <dd>{specialtyEntity.institutionId ? specialtyEntity.institutionId : ''}</dd>
        </dl>
        <Button tag={Link} to="/specialty" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/specialty/${specialtyEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ specialty }: IRootState) => ({
  specialtyEntity: specialty.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(SpecialtyDetail);
