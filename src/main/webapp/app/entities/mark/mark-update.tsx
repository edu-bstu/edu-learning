import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IStudent } from 'app/shared/model/student.model';
import { getEntities as getStudents } from 'app/entities/student/student.reducer';
import { ILesson } from 'app/shared/model/lesson.model';
import { getEntities as getLessons } from 'app/entities/lesson/lesson.reducer';
import { IExam } from 'app/shared/model/exam.model';
import { getEntities as getExams } from 'app/entities/exam/exam.reducer';
import { getEntity, updateEntity, createEntity, reset } from './mark.reducer';
import { IMark } from 'app/shared/model/mark.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IMarkUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const MarkUpdate = (props: IMarkUpdateProps) => {
  const [studentId, setStudentId] = useState('0');
  const [lessonId, setLessonId] = useState('0');
  const [examId, setExamId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { markEntity, students, lessons, exams, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/mark' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getStudents();
    props.getLessons();
    props.getExams();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...markEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="learningApp.mark.home.createOrEditLabel">
            <Translate contentKey="learningApp.mark.home.createOrEditLabel">Create or edit a Mark</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : markEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="mark-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="mark-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="markTypeLabel" for="mark-markType">
                  <Translate contentKey="learningApp.mark.markType">Mark Type</Translate>
                </Label>
                <AvInput
                  id="mark-markType"
                  type="select"
                  className="form-control"
                  name="markType"
                  value={(!isNew && markEntity.markType) || 'SCORE'}
                >
                  <option value="SCORE">{translate('learningApp.MarkType.SCORE')}</option>
                  <option value="ABSENCE">{translate('learningApp.MarkType.ABSENCE')}</option>
                  <option value="OFFSET">{translate('learningApp.MarkType.OFFSET')}</option>
                  <option value="NEGLECT">{translate('learningApp.MarkType.NEGLECT')}</option>
                </AvInput>
                <UncontrolledTooltip target="markTypeLabel">
                  <Translate contentKey="learningApp.mark.help.markType" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="scoreLabel" for="mark-score">
                  <Translate contentKey="learningApp.mark.score">Score</Translate>
                </Label>
                <AvField id="mark-score" type="string" className="form-control" name="score" />
                <UncontrolledTooltip target="scoreLabel">
                  <Translate contentKey="learningApp.mark.help.score" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="commentLabel" for="mark-comment">
                  <Translate contentKey="learningApp.mark.comment">Comment</Translate>
                </Label>
                <AvField id="mark-comment" type="text" name="comment" />
                <UncontrolledTooltip target="commentLabel">
                  <Translate contentKey="learningApp.mark.help.comment" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label for="mark-student">
                  <Translate contentKey="learningApp.mark.student">Student</Translate>
                </Label>
                <AvInput id="mark-student" type="select" className="form-control" name="studentId">
                  <option value="" key="0" />
                  {students
                    ? students.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="mark-lesson">
                  <Translate contentKey="learningApp.mark.lesson">Lesson</Translate>
                </Label>
                <AvInput id="mark-lesson" type="select" className="form-control" name="lessonId">
                  <option value="" key="0" />
                  {lessons
                    ? lessons.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="mark-exam">
                  <Translate contentKey="learningApp.mark.exam">Exam</Translate>
                </Label>
                <AvInput id="mark-exam" type="select" className="form-control" name="examId">
                  <option value="" key="0" />
                  {exams
                    ? exams.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/mark" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  students: storeState.student.entities,
  lessons: storeState.lesson.entities,
  exams: storeState.exam.entities,
  markEntity: storeState.mark.entity,
  loading: storeState.mark.loading,
  updating: storeState.mark.updating,
  updateSuccess: storeState.mark.updateSuccess,
});

const mapDispatchToProps = {
  getStudents,
  getLessons,
  getExams,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(MarkUpdate);
