import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './mark.reducer';
import { IMark } from 'app/shared/model/mark.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IMarkDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const MarkDetail = (props: IMarkDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { markEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="learningApp.mark.detail.title">Mark</Translate> [<b>{markEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="markType">
              <Translate contentKey="learningApp.mark.markType">Mark Type</Translate>
            </span>
            <UncontrolledTooltip target="markType">
              <Translate contentKey="learningApp.mark.help.markType" />
            </UncontrolledTooltip>
          </dt>
          <dd>{markEntity.markType}</dd>
          <dt>
            <span id="score">
              <Translate contentKey="learningApp.mark.score">Score</Translate>
            </span>
            <UncontrolledTooltip target="score">
              <Translate contentKey="learningApp.mark.help.score" />
            </UncontrolledTooltip>
          </dt>
          <dd>{markEntity.score}</dd>
          <dt>
            <span id="comment">
              <Translate contentKey="learningApp.mark.comment">Comment</Translate>
            </span>
            <UncontrolledTooltip target="comment">
              <Translate contentKey="learningApp.mark.help.comment" />
            </UncontrolledTooltip>
          </dt>
          <dd>{markEntity.comment}</dd>
          <dt>
            <Translate contentKey="learningApp.mark.student">Student</Translate>
          </dt>
          <dd>{markEntity.studentId ? markEntity.studentId : ''}</dd>
          <dt>
            <Translate contentKey="learningApp.mark.lesson">Lesson</Translate>
          </dt>
          <dd>{markEntity.lessonId ? markEntity.lessonId : ''}</dd>
          <dt>
            <Translate contentKey="learningApp.mark.exam">Exam</Translate>
          </dt>
          <dd>{markEntity.examId ? markEntity.examId : ''}</dd>
        </dl>
        <Button tag={Link} to="/mark" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/mark/${markEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ mark }: IRootState) => ({
  markEntity: mark.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(MarkDetail);
