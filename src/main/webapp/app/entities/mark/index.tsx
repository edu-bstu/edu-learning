import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Mark from './mark';
import MarkDetail from './mark-detail';
import MarkUpdate from './mark-update';
import MarkDeleteDialog from './mark-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={MarkUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={MarkUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={MarkDetail} />
      <ErrorBoundaryRoute path={match.url} component={Mark} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={MarkDeleteDialog} />
  </>
);

export default Routes;
