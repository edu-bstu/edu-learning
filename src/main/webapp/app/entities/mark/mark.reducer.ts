import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IMark, defaultValue } from 'app/shared/model/mark.model';

export const ACTION_TYPES = {
  FETCH_MARK_LIST: 'mark/FETCH_MARK_LIST',
  FETCH_MARK: 'mark/FETCH_MARK',
  CREATE_MARK: 'mark/CREATE_MARK',
  UPDATE_MARK: 'mark/UPDATE_MARK',
  DELETE_MARK: 'mark/DELETE_MARK',
  RESET: 'mark/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IMark>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type MarkState = Readonly<typeof initialState>;

// Reducer

export default (state: MarkState = initialState, action): MarkState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_MARK_LIST):
    case REQUEST(ACTION_TYPES.FETCH_MARK):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_MARK):
    case REQUEST(ACTION_TYPES.UPDATE_MARK):
    case REQUEST(ACTION_TYPES.DELETE_MARK):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_MARK_LIST):
    case FAILURE(ACTION_TYPES.FETCH_MARK):
    case FAILURE(ACTION_TYPES.CREATE_MARK):
    case FAILURE(ACTION_TYPES.UPDATE_MARK):
    case FAILURE(ACTION_TYPES.DELETE_MARK):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_MARK_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_MARK):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_MARK):
    case SUCCESS(ACTION_TYPES.UPDATE_MARK):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_MARK):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/marks';

// Actions

export const getEntities: ICrudGetAllAction<IMark> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_MARK_LIST,
    payload: axios.get<IMark>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IMark> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_MARK,
    payload: axios.get<IMark>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IMark> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_MARK,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IMark> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_MARK,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IMark> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_MARK,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
