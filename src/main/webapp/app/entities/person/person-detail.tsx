import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './person.reducer';
import { IPerson } from 'app/shared/model/person.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPersonDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PersonDetail = (props: IPersonDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { personEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="learningApp.person.detail.title">Person</Translate> [<b>{personEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="surname">
              <Translate contentKey="learningApp.person.surname">Surname</Translate>
            </span>
            <UncontrolledTooltip target="surname">
              <Translate contentKey="learningApp.person.help.surname" />
            </UncontrolledTooltip>
          </dt>
          <dd>{personEntity.surname}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="learningApp.person.name">Name</Translate>
            </span>
            <UncontrolledTooltip target="name">
              <Translate contentKey="learningApp.person.help.name" />
            </UncontrolledTooltip>
          </dt>
          <dd>{personEntity.name}</dd>
          <dt>
            <span id="patronymic">
              <Translate contentKey="learningApp.person.patronymic">Patronymic</Translate>
            </span>
            <UncontrolledTooltip target="patronymic">
              <Translate contentKey="learningApp.person.help.patronymic" />
            </UncontrolledTooltip>
          </dt>
          <dd>{personEntity.patronymic}</dd>
          <dt>
            <span id="dateOfBirth">
              <Translate contentKey="learningApp.person.dateOfBirth">Date Of Birth</Translate>
            </span>
            <UncontrolledTooltip target="dateOfBirth">
              <Translate contentKey="learningApp.person.help.dateOfBirth" />
            </UncontrolledTooltip>
          </dt>
          <dd>
            {personEntity.dateOfBirth ? <TextFormat value={personEntity.dateOfBirth} type="date" format={APP_LOCAL_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="email">
              <Translate contentKey="learningApp.person.email">Email</Translate>
            </span>
            <UncontrolledTooltip target="email">
              <Translate contentKey="learningApp.person.help.email" />
            </UncontrolledTooltip>
          </dt>
          <dd>{personEntity.email}</dd>
          <dt>
            <span id="phoneNumber">
              <Translate contentKey="learningApp.person.phoneNumber">Phone Number</Translate>
            </span>
            <UncontrolledTooltip target="phoneNumber">
              <Translate contentKey="learningApp.person.help.phoneNumber" />
            </UncontrolledTooltip>
          </dt>
          <dd>{personEntity.phoneNumber}</dd>
          <dt>
            <Translate contentKey="learningApp.person.user">User</Translate>
          </dt>
          <dd>{personEntity.userId ? personEntity.userId : ''}</dd>
        </dl>
        <Button tag={Link} to="/person" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/person/${personEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ person }: IRootState) => ({
  personEntity: person.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PersonDetail);
