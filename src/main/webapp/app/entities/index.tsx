import React from 'react';
import { Switch } from 'react-router-dom';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Subject from './subject';
import Lesson from './lesson';
import ScheduleLesson from './schedule-lesson';
import Mark from './mark';
import Exam from './exam';
import Person from './person';
import Student from './student';
import Employee from './employee';
import Institution from './institution';
import Specialty from './specialty';
import Group from './group';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}subject`} component={Subject} />
      <ErrorBoundaryRoute path={`${match.url}lesson`} component={Lesson} />
      <ErrorBoundaryRoute path={`${match.url}schedule-lesson`} component={ScheduleLesson} />
      <ErrorBoundaryRoute path={`${match.url}mark`} component={Mark} />
      <ErrorBoundaryRoute path={`${match.url}exam`} component={Exam} />
      <ErrorBoundaryRoute path={`${match.url}person`} component={Person} />
      <ErrorBoundaryRoute path={`${match.url}student`} component={Student} />
      <ErrorBoundaryRoute path={`${match.url}employee`} component={Employee} />
      <ErrorBoundaryRoute path={`${match.url}institution`} component={Institution} />
      <ErrorBoundaryRoute path={`${match.url}specialty`} component={Specialty} />
      <ErrorBoundaryRoute path={`${match.url}group`} component={Group} />
      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;
