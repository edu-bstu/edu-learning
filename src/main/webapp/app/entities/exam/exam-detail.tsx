import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './exam.reducer';
import { IExam } from 'app/shared/model/exam.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IExamDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ExamDetail = (props: IExamDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { examEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="learningApp.exam.detail.title">Exam</Translate> [<b>{examEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="dateTime">
              <Translate contentKey="learningApp.exam.dateTime">Date Time</Translate>
            </span>
            <UncontrolledTooltip target="dateTime">
              <Translate contentKey="learningApp.exam.help.dateTime" />
            </UncontrolledTooltip>
          </dt>
          <dd>{examEntity.dateTime ? <TextFormat value={examEntity.dateTime} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <Translate contentKey="learningApp.exam.subject">Subject</Translate>
          </dt>
          <dd>{examEntity.subjectId ? examEntity.subjectId : ''}</dd>
        </dl>
        <Button tag={Link} to="/exam" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/exam/${examEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ exam }: IRootState) => ({
  examEntity: exam.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ExamDetail);
