package ru.edu.bstu.learning.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import ru.edu.bstu.learning.web.rest.TestUtil;

public class ScheduleLessonTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ScheduleLesson.class);
        ScheduleLesson scheduleLesson1 = new ScheduleLesson();
        scheduleLesson1.setId(1L);
        ScheduleLesson scheduleLesson2 = new ScheduleLesson();
        scheduleLesson2.setId(scheduleLesson1.getId());
        assertThat(scheduleLesson1).isEqualTo(scheduleLesson2);
        scheduleLesson2.setId(2L);
        assertThat(scheduleLesson1).isNotEqualTo(scheduleLesson2);
        scheduleLesson1.setId(null);
        assertThat(scheduleLesson1).isNotEqualTo(scheduleLesson2);
    }
}
