package ru.edu.bstu.learning.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import ru.edu.bstu.learning.web.rest.TestUtil;

public class MarkTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Mark.class);
        Mark mark1 = new Mark();
        mark1.setId(1L);
        Mark mark2 = new Mark();
        mark2.setId(mark1.getId());
        assertThat(mark1).isEqualTo(mark2);
        mark2.setId(2L);
        assertThat(mark1).isNotEqualTo(mark2);
        mark1.setId(null);
        assertThat(mark1).isNotEqualTo(mark2);
    }
}
