package ru.edu.bstu.learning.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import ru.edu.bstu.learning.web.rest.TestUtil;

public class MarkDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MarkDTO.class);
        MarkDTO markDTO1 = new MarkDTO();
        markDTO1.setId(1L);
        MarkDTO markDTO2 = new MarkDTO();
        assertThat(markDTO1).isNotEqualTo(markDTO2);
        markDTO2.setId(markDTO1.getId());
        assertThat(markDTO1).isEqualTo(markDTO2);
        markDTO2.setId(2L);
        assertThat(markDTO1).isNotEqualTo(markDTO2);
        markDTO1.setId(null);
        assertThat(markDTO1).isNotEqualTo(markDTO2);
    }
}
