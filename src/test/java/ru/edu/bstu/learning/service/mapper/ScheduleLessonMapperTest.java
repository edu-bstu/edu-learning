package ru.edu.bstu.learning.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ScheduleLessonMapperTest {

    private ScheduleLessonMapper scheduleLessonMapper;

    @BeforeEach
    public void setUp() {
        scheduleLessonMapper = new ScheduleLessonMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(scheduleLessonMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(scheduleLessonMapper.fromId(null)).isNull();
    }
}
