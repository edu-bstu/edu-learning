package ru.edu.bstu.learning.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import ru.edu.bstu.learning.web.rest.TestUtil;

public class ScheduleLessonDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ScheduleLessonDTO.class);
        ScheduleLessonDTO scheduleLessonDTO1 = new ScheduleLessonDTO();
        scheduleLessonDTO1.setId(1L);
        ScheduleLessonDTO scheduleLessonDTO2 = new ScheduleLessonDTO();
        assertThat(scheduleLessonDTO1).isNotEqualTo(scheduleLessonDTO2);
        scheduleLessonDTO2.setId(scheduleLessonDTO1.getId());
        assertThat(scheduleLessonDTO1).isEqualTo(scheduleLessonDTO2);
        scheduleLessonDTO2.setId(2L);
        assertThat(scheduleLessonDTO1).isNotEqualTo(scheduleLessonDTO2);
        scheduleLessonDTO1.setId(null);
        assertThat(scheduleLessonDTO1).isNotEqualTo(scheduleLessonDTO2);
    }
}
