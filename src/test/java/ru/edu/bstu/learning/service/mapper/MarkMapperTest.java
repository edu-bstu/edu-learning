package ru.edu.bstu.learning.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class MarkMapperTest {

    private MarkMapper markMapper;

    @BeforeEach
    public void setUp() {
        markMapper = new MarkMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(markMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(markMapper.fromId(null)).isNull();
    }
}
