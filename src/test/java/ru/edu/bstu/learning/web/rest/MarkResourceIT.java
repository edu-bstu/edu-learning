package ru.edu.bstu.learning.web.rest;

import ru.edu.bstu.learning.LearningApp;
import ru.edu.bstu.learning.domain.Mark;
import ru.edu.bstu.learning.repository.MarkRepository;
import ru.edu.bstu.learning.service.MarkService;
import ru.edu.bstu.learning.service.dto.MarkDTO;
import ru.edu.bstu.learning.service.mapper.MarkMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import ru.edu.bstu.learning.domain.enumeration.MarkType;
/**
 * Integration tests for the {@link MarkResource} REST controller.
 */
@SpringBootTest(classes = LearningApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class MarkResourceIT {

    private static final MarkType DEFAULT_MARK_TYPE = MarkType.SCORE;
    private static final MarkType UPDATED_MARK_TYPE = MarkType.ABSENCE;

    private static final Integer DEFAULT_SCORE = 1;
    private static final Integer UPDATED_SCORE = 2;

    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    @Autowired
    private MarkRepository markRepository;

    @Autowired
    private MarkMapper markMapper;

    @Autowired
    private MarkService markService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMarkMockMvc;

    private Mark mark;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Mark createEntity(EntityManager em) {
        Mark mark = new Mark()
            .markType(DEFAULT_MARK_TYPE)
            .score(DEFAULT_SCORE)
            .comment(DEFAULT_COMMENT);
        return mark;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Mark createUpdatedEntity(EntityManager em) {
        Mark mark = new Mark()
            .markType(UPDATED_MARK_TYPE)
            .score(UPDATED_SCORE)
            .comment(UPDATED_COMMENT);
        return mark;
    }

    @BeforeEach
    public void initTest() {
        mark = createEntity(em);
    }

    @Test
    @Transactional
    public void createMark() throws Exception {
        int databaseSizeBeforeCreate = markRepository.findAll().size();
        // Create the Mark
        MarkDTO markDTO = markMapper.toDto(mark);
        restMarkMockMvc.perform(post("/api/marks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(markDTO)))
            .andExpect(status().isCreated());

        // Validate the Mark in the database
        List<Mark> markList = markRepository.findAll();
        assertThat(markList).hasSize(databaseSizeBeforeCreate + 1);
        Mark testMark = markList.get(markList.size() - 1);
        assertThat(testMark.getMarkType()).isEqualTo(DEFAULT_MARK_TYPE);
        assertThat(testMark.getScore()).isEqualTo(DEFAULT_SCORE);
        assertThat(testMark.getComment()).isEqualTo(DEFAULT_COMMENT);
    }

    @Test
    @Transactional
    public void createMarkWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = markRepository.findAll().size();

        // Create the Mark with an existing ID
        mark.setId(1L);
        MarkDTO markDTO = markMapper.toDto(mark);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMarkMockMvc.perform(post("/api/marks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(markDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Mark in the database
        List<Mark> markList = markRepository.findAll();
        assertThat(markList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkMarkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = markRepository.findAll().size();
        // set the field null
        mark.setMarkType(null);

        // Create the Mark, which fails.
        MarkDTO markDTO = markMapper.toDto(mark);


        restMarkMockMvc.perform(post("/api/marks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(markDTO)))
            .andExpect(status().isBadRequest());

        List<Mark> markList = markRepository.findAll();
        assertThat(markList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMarks() throws Exception {
        // Initialize the database
        markRepository.saveAndFlush(mark);

        // Get all the markList
        restMarkMockMvc.perform(get("/api/marks?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mark.getId().intValue())))
            .andExpect(jsonPath("$.[*].markType").value(hasItem(DEFAULT_MARK_TYPE.toString())))
            .andExpect(jsonPath("$.[*].score").value(hasItem(DEFAULT_SCORE)))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT)));
    }
    
    @Test
    @Transactional
    public void getMark() throws Exception {
        // Initialize the database
        markRepository.saveAndFlush(mark);

        // Get the mark
        restMarkMockMvc.perform(get("/api/marks/{id}", mark.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(mark.getId().intValue()))
            .andExpect(jsonPath("$.markType").value(DEFAULT_MARK_TYPE.toString()))
            .andExpect(jsonPath("$.score").value(DEFAULT_SCORE))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT));
    }
    @Test
    @Transactional
    public void getNonExistingMark() throws Exception {
        // Get the mark
        restMarkMockMvc.perform(get("/api/marks/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMark() throws Exception {
        // Initialize the database
        markRepository.saveAndFlush(mark);

        int databaseSizeBeforeUpdate = markRepository.findAll().size();

        // Update the mark
        Mark updatedMark = markRepository.findById(mark.getId()).get();
        // Disconnect from session so that the updates on updatedMark are not directly saved in db
        em.detach(updatedMark);
        updatedMark
            .markType(UPDATED_MARK_TYPE)
            .score(UPDATED_SCORE)
            .comment(UPDATED_COMMENT);
        MarkDTO markDTO = markMapper.toDto(updatedMark);

        restMarkMockMvc.perform(put("/api/marks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(markDTO)))
            .andExpect(status().isOk());

        // Validate the Mark in the database
        List<Mark> markList = markRepository.findAll();
        assertThat(markList).hasSize(databaseSizeBeforeUpdate);
        Mark testMark = markList.get(markList.size() - 1);
        assertThat(testMark.getMarkType()).isEqualTo(UPDATED_MARK_TYPE);
        assertThat(testMark.getScore()).isEqualTo(UPDATED_SCORE);
        assertThat(testMark.getComment()).isEqualTo(UPDATED_COMMENT);
    }

    @Test
    @Transactional
    public void updateNonExistingMark() throws Exception {
        int databaseSizeBeforeUpdate = markRepository.findAll().size();

        // Create the Mark
        MarkDTO markDTO = markMapper.toDto(mark);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMarkMockMvc.perform(put("/api/marks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(markDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Mark in the database
        List<Mark> markList = markRepository.findAll();
        assertThat(markList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMark() throws Exception {
        // Initialize the database
        markRepository.saveAndFlush(mark);

        int databaseSizeBeforeDelete = markRepository.findAll().size();

        // Delete the mark
        restMarkMockMvc.perform(delete("/api/marks/{id}", mark.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Mark> markList = markRepository.findAll();
        assertThat(markList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
