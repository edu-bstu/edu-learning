package ru.edu.bstu.learning.web.rest;

import ru.edu.bstu.learning.LearningApp;
import ru.edu.bstu.learning.domain.ScheduleLesson;
import ru.edu.bstu.learning.repository.ScheduleLessonRepository;
import ru.edu.bstu.learning.service.ScheduleLessonService;
import ru.edu.bstu.learning.service.dto.ScheduleLessonDTO;
import ru.edu.bstu.learning.service.mapper.ScheduleLessonMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ScheduleLessonResource} REST controller.
 */
@SpringBootTest(classes = LearningApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ScheduleLessonResourceIT {

    private static final Instant DEFAULT_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_DAY = 1;
    private static final Integer UPDATED_DAY = 2;

    private static final Integer DEFAULT_NUMBER = 1;
    private static final Integer UPDATED_NUMBER = 2;

    private static final Boolean DEFAULT_NUMERATOR = false;
    private static final Boolean UPDATED_NUMERATOR = true;

    @Autowired
    private ScheduleLessonRepository scheduleLessonRepository;

    @Autowired
    private ScheduleLessonMapper scheduleLessonMapper;

    @Autowired
    private ScheduleLessonService scheduleLessonService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restScheduleLessonMockMvc;

    private ScheduleLesson scheduleLesson;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ScheduleLesson createEntity(EntityManager em) {
        ScheduleLesson scheduleLesson = new ScheduleLesson()
            .time(DEFAULT_TIME)
            .day(DEFAULT_DAY)
            .number(DEFAULT_NUMBER)
            .numerator(DEFAULT_NUMERATOR);
        return scheduleLesson;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ScheduleLesson createUpdatedEntity(EntityManager em) {
        ScheduleLesson scheduleLesson = new ScheduleLesson()
            .time(UPDATED_TIME)
            .day(UPDATED_DAY)
            .number(UPDATED_NUMBER)
            .numerator(UPDATED_NUMERATOR);
        return scheduleLesson;
    }

    @BeforeEach
    public void initTest() {
        scheduleLesson = createEntity(em);
    }

    @Test
    @Transactional
    public void createScheduleLesson() throws Exception {
        int databaseSizeBeforeCreate = scheduleLessonRepository.findAll().size();
        // Create the ScheduleLesson
        ScheduleLessonDTO scheduleLessonDTO = scheduleLessonMapper.toDto(scheduleLesson);
        restScheduleLessonMockMvc.perform(post("/api/schedule-lessons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(scheduleLessonDTO)))
            .andExpect(status().isCreated());

        // Validate the ScheduleLesson in the database
        List<ScheduleLesson> scheduleLessonList = scheduleLessonRepository.findAll();
        assertThat(scheduleLessonList).hasSize(databaseSizeBeforeCreate + 1);
        ScheduleLesson testScheduleLesson = scheduleLessonList.get(scheduleLessonList.size() - 1);
        assertThat(testScheduleLesson.getTime()).isEqualTo(DEFAULT_TIME);
        assertThat(testScheduleLesson.getDay()).isEqualTo(DEFAULT_DAY);
        assertThat(testScheduleLesson.getNumber()).isEqualTo(DEFAULT_NUMBER);
        assertThat(testScheduleLesson.isNumerator()).isEqualTo(DEFAULT_NUMERATOR);
    }

    @Test
    @Transactional
    public void createScheduleLessonWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = scheduleLessonRepository.findAll().size();

        // Create the ScheduleLesson with an existing ID
        scheduleLesson.setId(1L);
        ScheduleLessonDTO scheduleLessonDTO = scheduleLessonMapper.toDto(scheduleLesson);

        // An entity with an existing ID cannot be created, so this API call must fail
        restScheduleLessonMockMvc.perform(post("/api/schedule-lessons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(scheduleLessonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ScheduleLesson in the database
        List<ScheduleLesson> scheduleLessonList = scheduleLessonRepository.findAll();
        assertThat(scheduleLessonList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = scheduleLessonRepository.findAll().size();
        // set the field null
        scheduleLesson.setTime(null);

        // Create the ScheduleLesson, which fails.
        ScheduleLessonDTO scheduleLessonDTO = scheduleLessonMapper.toDto(scheduleLesson);


        restScheduleLessonMockMvc.perform(post("/api/schedule-lessons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(scheduleLessonDTO)))
            .andExpect(status().isBadRequest());

        List<ScheduleLesson> scheduleLessonList = scheduleLessonRepository.findAll();
        assertThat(scheduleLessonList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDayIsRequired() throws Exception {
        int databaseSizeBeforeTest = scheduleLessonRepository.findAll().size();
        // set the field null
        scheduleLesson.setDay(null);

        // Create the ScheduleLesson, which fails.
        ScheduleLessonDTO scheduleLessonDTO = scheduleLessonMapper.toDto(scheduleLesson);


        restScheduleLessonMockMvc.perform(post("/api/schedule-lessons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(scheduleLessonDTO)))
            .andExpect(status().isBadRequest());

        List<ScheduleLesson> scheduleLessonList = scheduleLessonRepository.findAll();
        assertThat(scheduleLessonList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = scheduleLessonRepository.findAll().size();
        // set the field null
        scheduleLesson.setNumber(null);

        // Create the ScheduleLesson, which fails.
        ScheduleLessonDTO scheduleLessonDTO = scheduleLessonMapper.toDto(scheduleLesson);


        restScheduleLessonMockMvc.perform(post("/api/schedule-lessons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(scheduleLessonDTO)))
            .andExpect(status().isBadRequest());

        List<ScheduleLesson> scheduleLessonList = scheduleLessonRepository.findAll();
        assertThat(scheduleLessonList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumeratorIsRequired() throws Exception {
        int databaseSizeBeforeTest = scheduleLessonRepository.findAll().size();
        // set the field null
        scheduleLesson.setNumerator(null);

        // Create the ScheduleLesson, which fails.
        ScheduleLessonDTO scheduleLessonDTO = scheduleLessonMapper.toDto(scheduleLesson);


        restScheduleLessonMockMvc.perform(post("/api/schedule-lessons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(scheduleLessonDTO)))
            .andExpect(status().isBadRequest());

        List<ScheduleLesson> scheduleLessonList = scheduleLessonRepository.findAll();
        assertThat(scheduleLessonList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllScheduleLessons() throws Exception {
        // Initialize the database
        scheduleLessonRepository.saveAndFlush(scheduleLesson);

        // Get all the scheduleLessonList
        restScheduleLessonMockMvc.perform(get("/api/schedule-lessons?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(scheduleLesson.getId().intValue())))
            .andExpect(jsonPath("$.[*].time").value(hasItem(DEFAULT_TIME.toString())))
            .andExpect(jsonPath("$.[*].day").value(hasItem(DEFAULT_DAY)))
            .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER)))
            .andExpect(jsonPath("$.[*].numerator").value(hasItem(DEFAULT_NUMERATOR.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getScheduleLesson() throws Exception {
        // Initialize the database
        scheduleLessonRepository.saveAndFlush(scheduleLesson);

        // Get the scheduleLesson
        restScheduleLessonMockMvc.perform(get("/api/schedule-lessons/{id}", scheduleLesson.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(scheduleLesson.getId().intValue()))
            .andExpect(jsonPath("$.time").value(DEFAULT_TIME.toString()))
            .andExpect(jsonPath("$.day").value(DEFAULT_DAY))
            .andExpect(jsonPath("$.number").value(DEFAULT_NUMBER))
            .andExpect(jsonPath("$.numerator").value(DEFAULT_NUMERATOR.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingScheduleLesson() throws Exception {
        // Get the scheduleLesson
        restScheduleLessonMockMvc.perform(get("/api/schedule-lessons/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateScheduleLesson() throws Exception {
        // Initialize the database
        scheduleLessonRepository.saveAndFlush(scheduleLesson);

        int databaseSizeBeforeUpdate = scheduleLessonRepository.findAll().size();

        // Update the scheduleLesson
        ScheduleLesson updatedScheduleLesson = scheduleLessonRepository.findById(scheduleLesson.getId()).get();
        // Disconnect from session so that the updates on updatedScheduleLesson are not directly saved in db
        em.detach(updatedScheduleLesson);
        updatedScheduleLesson
            .time(UPDATED_TIME)
            .day(UPDATED_DAY)
            .number(UPDATED_NUMBER)
            .numerator(UPDATED_NUMERATOR);
        ScheduleLessonDTO scheduleLessonDTO = scheduleLessonMapper.toDto(updatedScheduleLesson);

        restScheduleLessonMockMvc.perform(put("/api/schedule-lessons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(scheduleLessonDTO)))
            .andExpect(status().isOk());

        // Validate the ScheduleLesson in the database
        List<ScheduleLesson> scheduleLessonList = scheduleLessonRepository.findAll();
        assertThat(scheduleLessonList).hasSize(databaseSizeBeforeUpdate);
        ScheduleLesson testScheduleLesson = scheduleLessonList.get(scheduleLessonList.size() - 1);
        assertThat(testScheduleLesson.getTime()).isEqualTo(UPDATED_TIME);
        assertThat(testScheduleLesson.getDay()).isEqualTo(UPDATED_DAY);
        assertThat(testScheduleLesson.getNumber()).isEqualTo(UPDATED_NUMBER);
        assertThat(testScheduleLesson.isNumerator()).isEqualTo(UPDATED_NUMERATOR);
    }

    @Test
    @Transactional
    public void updateNonExistingScheduleLesson() throws Exception {
        int databaseSizeBeforeUpdate = scheduleLessonRepository.findAll().size();

        // Create the ScheduleLesson
        ScheduleLessonDTO scheduleLessonDTO = scheduleLessonMapper.toDto(scheduleLesson);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restScheduleLessonMockMvc.perform(put("/api/schedule-lessons")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(scheduleLessonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ScheduleLesson in the database
        List<ScheduleLesson> scheduleLessonList = scheduleLessonRepository.findAll();
        assertThat(scheduleLessonList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteScheduleLesson() throws Exception {
        // Initialize the database
        scheduleLessonRepository.saveAndFlush(scheduleLesson);

        int databaseSizeBeforeDelete = scheduleLessonRepository.findAll().size();

        // Delete the scheduleLesson
        restScheduleLessonMockMvc.perform(delete("/api/schedule-lessons/{id}", scheduleLesson.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ScheduleLesson> scheduleLessonList = scheduleLessonRepository.findAll();
        assertThat(scheduleLessonList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
